* New 4.0 version introduced with all code moved to Kotlin and updated to the current standards
* Removed unused permissions
* Dependencies cleanup
* Translations updated
* New libraries version
* A lot of code cleanup
* New import engine with new formats (SDM, SudoCue, Puzzle Bank)
* Bugfixes (last played time, visual keyboard, menu items, command stack imports, undo button)
* New features:
** double pencil marks (central and corner, can be disabled in Settings)
** new modes of invalid value check (compare to final solution)
** invalid puzzle check (for editor and import) verifies if puzzle has exactly one solution
** mistake counter
** prevent duplicate puzzles on import
** copy puzzle to clipboard
** tones played on invalid entry and finished digit (can be disabled in top menu)
** single puzzle export option
** new Polish language translation
** import/export speed-up and fixes, supports big amounts now
** visual effects and icons updates