/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.util.AttributeSet
import android.view.KeyEvent
import android.view.MotionEvent
import android.view.View
import android.view.animation.AlphaAnimation
import android.view.animation.Animation
import com.google.android.material.color.MaterialColors
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.CellMarks
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SameValueHighlightMode
import org.moire.opensudoku.game.SudokuBoard
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.utils.splitToN
import kotlin.math.min
import kotlin.math.roundToInt

/**
 * Sudoku board widget.
 */
class SudokuBoardView(context: Context, attrs: AttributeSet? = null) : View(context, attrs) {
	private var blinkingDigit: Int = 0
	private val blinker = Blinker(this)
	var selectedCell: Cell? = null
		private set(value) {
			if (field !== value) {
				field = value
				onCellSelectedListener(value)
			}
		}

	private var cellWidth = 0f
	private var cellHeight = 0f
	private var touchedCell: Cell? = null
	internal var highlightedValue = 0
		set(value) {
			field = value
			postInvalidate()
		}
	internal var highlightDirectlyWrongValues = true
		set(value) {
			field = value
			postInvalidate()
		}
	internal var highlightIndirectlyWrongValues = true
		set(value) {
			field = value
			postInvalidate()
		}
	internal var highlightTouchedCell = true
	internal var autoHideTouchedCellHint = true
	internal var sameValueHighlightMode = SameValueHighlightMode.NONE
		set(value) {
			field = value
			postInvalidate()
		}
	internal var isDoubleMarksEnabled: Boolean = true

	private lateinit var game: SudokuGame
	private lateinit var _board: SudokuBoard
	private var isInitialized = false
	var onReadonlyChangeListener: (() -> Unit)? = null

	/**
	 * Registers callback which will be invoked when user taps the cell.
	 */
	internal var onCellTappedListener: (Cell) -> Unit = {}

	/**
	 * Callback invoked when cell is selected. Cell selection can change without user interaction.
	 */
	internal lateinit var onCellSelectedListener: (Cell?) -> Unit

	/**
	 * Stores the background and foreground paints for each cell so they can be drawn
	 * in one pass. First two indices are the cell's row and column, the last one is
	 * paints for the cell's background, text, or marks text, respectively.
	 *
	 *
	 * So [1][3][0] is the background ([0]) paint of the cell in the second ([1]) row
	 * in the fourth ([3]) column.
	 */
	private var paints: Array<Array<Array<Paint>>>
	val value = Paint()
	val textMark = Paint()
	val textFocused = Paint()
	val textMarkFocused = Paint()
	val textHighlighted = Paint()
	val textMarkHighlighted = Paint()
	val textReadOnly = Paint()
	val textEvenCells = Paint()
	val textEvenCellsMarks = Paint()
	val textTouched = Paint()
	val textTouchedMarks = Paint()
	val textInvalid = Paint()
	val background = Paint()
	val backgroundEvenCells = Paint()
	val backgroundReadOnly = Paint()
	val backgroundTouched = Paint()
	val backgroundHighlighted = Paint()
	val backgroundInvalid = Paint()
	val selectedCellFrame = Paint()
	val line = Paint()
	val sectorLine = Paint()

	private val bounds = Rect()

	private var numberLeft = 0
	private var numberTop = 0
	private var marksTop = 0f
	private var sectorLineWidth = 0

	/** Move the cell focus to the right if a value (not mark) is entered  */
	internal var moveCellSelectionOnPress = false

	init {
		isFocusable = true
		isFocusableInTouchMode = true
		value.isAntiAlias = true
		textReadOnly.isAntiAlias = true
		textInvalid.isAntiAlias = true
		textEvenCells.isAntiAlias = true
		textTouched.isAntiAlias = true
		textFocused.isAntiAlias = true
		textHighlighted.isAntiAlias = true
		textMark.isAntiAlias = true
		textEvenCellsMarks.isAntiAlias = true
		textMarkFocused.isAntiAlias = true
		textTouchedMarks.isAntiAlias = true
		textMarkHighlighted.isAntiAlias = true
		paints = Array(9) { Array(9) { arrayOf(background, value, textMark) } }
		setAllColorsFromThemedContext(context)
		contentDescription = context.getString(R.string.sudoku_board_widget)
	}

	override fun onDraw(canvas: Canvas) {
		super.onDraw(canvas)

		if (!isInitialized) return
		val width = width - paddingRight
		val height = height - paddingBottom
		val paddingLeft = paddingLeft
		val paddingTop = paddingTop

		// Ensure the whole canvas starts with the background colour, in case any other colours are transparent.
		canvas.drawRect(0f, 0f, width.toFloat(), height.toFloat(), background)

		// draw cells
		var cellLeft: Int
		var cellTop: Int
		val numberAscent = value.ascent()
		val marksAscent = textMark.ascent()
		if (highlightIndirectlyWrongValues) {
			board.solutionCount // make sure solution is known for cells to use for highlighting
		}
		for (row in 0..8) {
			for (col in 0..8) {
				// Default colours for ordinary cells
				paints[row][col][0] = background
				paints[row][col][1] = value
				paints[row][col][2] = textMark
				val cell = board.getCell(row, col)

				// Even boxes
				if (backgroundEvenCells.color != Color.TRANSPARENT) {
					val boxNumber = row / 3 + col / 3 + 1 // 1-based
					if (boxNumber % 2 == 0) {
						paints[row][col][0] = backgroundEvenCells
						paints[row][col][1] = textEvenCells
						paints[row][col][2] = textEvenCellsMarks
					} else {
						paints[row][col][0] = background
						paints[row][col][1] = value
						paints[row][col][2] = textMark
					}
				}

				// Read-only (given digit) cells
				if (!cell.isEditable) {
					paints[row][col][0] = backgroundReadOnly
					paints[row][col][1] = textReadOnly
				}

				if (shouldHighlightCell(cell)) {
					paints[row][col][0] = backgroundHighlighted
					if (cell.isEditable) paints[row][col][1] = textHighlighted
					paints[row][col][2] = textMarkHighlighted
				}

				// Seeing that a cell is invalid is more important than it being highlighted. Only mark editable cells as errors, there's no point
				// marking a given cell with an error (partly because the user can't change it, and partly because then the user can't easily see which
				// of the cells containing the error are editable).
				if (highlightDirectlyWrongValues && cell.value != 0 && (cell.isEditable || board.isEditMode) && !cell.isValid) {
					paints[row][col][0] = backgroundInvalid
					paints[row][col][1] = textInvalid
					paints[row][col][2] = textMark // Not read, set to avoid risk of NPEs
				}
				if (highlightIndirectlyWrongValues && cell.value != 0 && !cell.isCorrect) {
					paints[row][col][0] = backgroundInvalid
					paints[row][col][1] = textInvalid
					paints[row][col][2] = textMark // Not read, set to avoid risk of NPEs
				}

				// Highlight this cell if (a) we're highlighting cells in the same row/column as the touched cell, and (b) this cell is in that row or column.
				if ((highlightTouchedCell && (row == touchedCell?.rowIndex || col == touchedCell?.columnIndex))
					|| (sameValueHighlightMode == SameValueHighlightMode.HINTS_ONLY && cell.hintHighlight == HintHighlight.CAUSE)
				) {
					paints[row][col][0] = backgroundTouched
					paints[row][col][1] = textTouched
					paints[row][col][2] = textTouchedMarks
				}

				// Draw the cell background
				cellLeft = (col * cellWidth + paddingLeft).roundToInt()
				cellTop = (row * cellHeight + paddingTop).roundToInt()
				canvas.drawRect(cellLeft.toFloat(), cellTop.toFloat(), cellLeft + cellWidth, cellTop + cellHeight, paints[row][col][0])

				// Draw cell contents
				val value = cell.value
				if (value != 0) {
					if (value != blinkingDigit) { // draw the digit if not in the blinking phase, otherwise hide the digit
						canvas.drawText("$value", (cellLeft + numberLeft).toFloat(), cellTop + numberTop - numberAscent, paints[row][col][1])
					}
				} else {
					// Each cell is divided to 3 rows for all marks drawing
					if (!isDoubleMarksEnabled) {
						for (number in cell.centralMarks.marksValues) {
							val numberOfCols = 3
							val markRow = (number - 1) / numberOfCols
							val markCol = (number - 1) % numberOfCols
							val markNumber = "$number"
							val charWidth = paints[row][col][2].getCharWidth(markNumber)
							val markSpaceWidth = cellWidth / (numberOfCols + 1f)
							val posX = cellLeft + (markCol + 1f) * markSpaceWidth - charWidth / 2f
							val posY = cellTop + marksTop - marksAscent + markRow * paints[row][col][2].textSize

							if (number == highlightedValue && sameValueHighlightMode == SameValueHighlightMode.NUMBERS_AND_MARK_VALUES) {
								canvas.drawCircle(
									posX + charWidth / 2f,
									posY - paints[row][col][2].textSize * 0.38f,
									paints[row][col][2].textSize * 0.6f,
									backgroundHighlighted
								)
							}

							canvas.drawText(markNumber, posX, posY, paints[row][col][2])
						}
					} else {
						if (!cell.centralMarks.isEmpty) { // central marks are drawn in the 2nd row only, centre-aligned.
							drawMarks(paints[row][col][2], cell.centralMarks.marksValues, canvas, cellTop, cellLeft, MarkPlacement.CENTER)
						}
						if (!cell.cornerMarks.isEmpty) { // corner marks go to 1st and 3rd row corners
							cell.cornerMarks.marksValues.sort()
							val splitMarks = cell.cornerMarks.marksValues.splitToN(4)
							drawMarks(paints[row][col][2], splitMarks[0], canvas, cellTop, cellLeft, MarkPlacement.TOP_LEFT)
							drawMarks(paints[row][col][2], splitMarks[1], canvas, cellTop, cellLeft, MarkPlacement.TOP_RIGHT)
							drawMarks(paints[row][col][2], splitMarks[2], canvas, cellTop, cellLeft, MarkPlacement.BOTTOM_LEFT)
							drawMarks(paints[row][col][2], splitMarks[3], canvas, cellTop, cellLeft, MarkPlacement.BOTTOM_RIGHT)
						}
					}
				}

				// highlight selected cell
				if ((!isReadOnlyPreview && cell == selectedCell)
					|| (sameValueHighlightMode == SameValueHighlightMode.HINTS_ONLY && cell.hintHighlight == HintHighlight.TARGET)
				) {
					cellLeft = (col * cellWidth).roundToInt() + paddingLeft
					cellTop = (row * cellHeight).roundToInt() + paddingTop

					// The stroke is drawn half inside and half outside the given cell. Compensate by adjusting the cell's bounds by half the
					// stroke width to move it entirely inside the cell.
					val halfStrokeWidth = selectedCellFrame.strokeWidth / 2
					selectedCellFrame.alpha = 128
					canvas.drawRect(
						cellLeft + halfStrokeWidth,
						cellTop + halfStrokeWidth,
						cellLeft + cellWidth - halfStrokeWidth,
						cellTop + cellHeight - halfStrokeWidth,
						selectedCellFrame
					)
				}
			}
		}

		// draw vertical lines
		for (c in 0..9) {
			val x = c * cellWidth + paddingLeft
			canvas.drawLine(x, paddingTop.toFloat(), x, height.toFloat(), line)
		}

		// draw horizontal lines
		for (rowIndex in 0..9) {
			val y = rowIndex * cellHeight + paddingTop
			canvas.drawLine(paddingLeft.toFloat(), y, width.toFloat(), y, line)
		}
		val sectorLineWidth1 = sectorLineWidth / 2
		val sectorLineWidth2 = sectorLineWidth1 + sectorLineWidth % 2

		// draw sector (thick) lines
		var c = 0
		while (c <= 9) {
			val x = c * cellWidth + paddingLeft
			canvas.drawRect(x - sectorLineWidth1, paddingTop.toFloat(), x + sectorLineWidth2, height.toFloat(), sectorLine)
			c += 3
		}
		var r = 0
		while (r <= 9) {
			val y = r * cellHeight + paddingTop
			canvas.drawRect(paddingLeft.toFloat(), y - sectorLineWidth1, width.toFloat(), y + sectorLineWidth2, sectorLine)
			r += 3
		}
	}

	private fun shouldHighlightCell(cell: Cell): Boolean {
		return when (sameValueHighlightMode) {
			SameValueHighlightMode.NUMBERS, SameValueHighlightMode.NUMBERS_AND_MARK_VALUES -> {
				highlightedValue != 0 && cell.value == highlightedValue
			}

			SameValueHighlightMode.NUMBERS_AND_MARK_CELLS -> {
				if (cell.value != 0) {
					cell.value == highlightedValue
				} else {
					cell.centralMarks.hasNumber(highlightedValue) || cell.cornerMarks.hasNumber(highlightedValue)
				}
			}

			SameValueHighlightMode.HINTS_ONLY -> {
				cell.hintHighlight == HintHighlight.REGION
			}

			else -> {
				false
			}
		}
	}

	private fun drawMarks(paintSrc: Paint, marks: List<Int>, canvas: Canvas, cellTop: Int, cellLeft: Int, placement: MarkPlacement) {
		if (marks.isEmpty()) {
			return
		}

		val paint = Paint()
		paint.set(paintSrc)
		paint.textAlign = placement.textAlign
		val marksText = marks.joinToString("")

		// Determine the font size to use
		paint.getTextBounds(marksText, 0, marksText.length, bounds)
		if (bounds.width() / cellWidth > 0.97f) { // if necessary scale down the size of text, and recalculate the bounds
			paint.textSize = paint.textSize * 0.97f * cellWidth / bounds.width()
			paint.getTextBounds(marksText, 0, marksText.length, bounds)
		}

		// Horizontally align the text
		val offsetX = cellWidth * placement.xm

		// Vertically align the central marks
		val offsetY = bounds.height() + placement.ym * (cellHeight - bounds.height())

		if (sameValueHighlightMode == SameValueHighlightMode.NUMBERS_AND_MARK_VALUES) {
			marks.forEachIndexed { index, value ->
				if (value == highlightedValue) {
					var valueXOffset = bounds.width().toFloat() / marks.size.toFloat() * (index.toFloat() + 0.5f)
					valueXOffset = when (placement.textAlign) {
						Paint.Align.CENTER -> offsetX + valueXOffset - bounds.width() / 2f
						Paint.Align.LEFT -> offsetX + valueXOffset + 1
						Paint.Align.RIGHT -> offsetX + valueXOffset - bounds.width() - 1
					}
					canvas.drawCircle(
						cellLeft + valueXOffset,
						cellTop + offsetY - bounds.height() / 2f + 1,
						bounds.height() * 0.7f,
						backgroundHighlighted
					)
				}
			}
		}
		canvas.drawText(marksText, cellLeft + offsetX, cellTop + offsetY, paint)
	}

	fun setAllColorsFromThemedContext(context: Context) {
		// Grid lines
		line.color = MaterialColors.getColor(context, R.attr.colorLine, Color.BLACK)
		sectorLine.color = MaterialColors.getColor(context, R.attr.colorSectorLine, Color.BLACK)

		// Normal cell
		value.color = MaterialColors.getColor(context, R.attr.colorValueText, Color.BLACK)
		textMark.color = MaterialColors.getColor(context, R.attr.colorMarksText, Color.BLACK)
		background.color = MaterialColors.getColor(context, R.attr.colorBackground, Color.WHITE)

		// Default view behaviour is to highlight a view that has the focus. This highlights the entire board and leads to incorrect colours.
		defaultFocusHighlightEnabled = false

		// Read-only cell
		textReadOnly.color = MaterialColors.getColor(context, R.attr.colorReadOnlyText, Color.WHITE)
		backgroundReadOnly.color = MaterialColors.getColor(context, R.attr.colorReadOnlyBackground, Color.RED)

		// Even 3x3 boxes
		textEvenCells.color = MaterialColors.getColor(context, R.attr.colorEvenText, Color.BLACK)
		textEvenCellsMarks.color = MaterialColors.getColor(context, R.attr.colorEvenText, Color.BLACK)
		backgroundEvenCells.color = MaterialColors.getColor(context, R.attr.colorEvenBackground, Color.TRANSPARENT)

		// Touched
		textTouched.color = MaterialColors.getColor(context, R.attr.colorTouchedText, Color.BLACK)
		textTouchedMarks.color = MaterialColors.getColor(context, R.attr.colorTouchedMarksText, Color.BLACK)
		backgroundTouched.color = MaterialColors.getColor(context, R.attr.colorTouchedBackground, Color.WHITE)

		// Selected / focused
		selectedCellFrame.color = (MaterialColors.getColor(context, R.attr.colorSelectedCellFrame, Color.WHITE))

		// Highlighted cell
		backgroundHighlighted.color = MaterialColors.getColor(context, R.attr.colorHighlightedBackground, Color.WHITE)
		textHighlighted.color = MaterialColors.getColor(context, R.attr.colorHighlightedText, Color.BLACK)
		textMarkHighlighted.color = MaterialColors.getColor(context, R.attr.colorHighlightedMarksText, Color.BLACK)

		// Invalid values
		textInvalid.color = MaterialColors.getColor(context, R.attr.colorInvalidText, Color.WHITE)
		backgroundInvalid.color = MaterialColors.getColor(context, R.attr.colorInvalidBackground, Color.BLACK)
	}

	fun setGame(game: SudokuGame) {
		this.game = game
		board = game.board
	}

	var board: SudokuBoard
		get() = _board
		set(newBoard) {
			_board = newBoard
			_board.ensureOnChangeListener(this::postInvalidate)
			isInitialized = true
			postInvalidate()
		}

	var isReadOnlyPreview: Boolean = false
		set(newValue) {
			if (field != newValue) {
				field = newValue
				postInvalidate()
				onReadonlyChangeListener?.invoke()
			}
		}

	fun hideTouchedCellHint() {
		touchedCell = null
		postInvalidate()
	}

	fun invokeOnCellSelected() {
		onCellSelectedListener(selectedCell)
	}

	override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
		val widthMode = MeasureSpec.getMode(widthMeasureSpec)
		val widthSize = MeasureSpec.getSize(widthMeasureSpec)
		val heightMode = MeasureSpec.getMode(heightMeasureSpec)
		val heightSize = MeasureSpec.getSize(heightMeasureSpec)
		var width: Int
		var height: Int
		if (widthMode == MeasureSpec.EXACTLY) {
			width = widthSize
		} else {
			width = DEFAULT_BOARD_SIZE
			if (widthMode == MeasureSpec.AT_MOST && width > widthSize) {
				width = widthSize
			}
		}
		if (heightMode == MeasureSpec.EXACTLY) {
			height = heightSize
		} else {
			height = DEFAULT_BOARD_SIZE
			if (heightMode == MeasureSpec.AT_MOST && height > heightSize) {
				height = heightSize
			}
		}
		if (widthMode != MeasureSpec.EXACTLY) {
			width = height
		}
		if (heightMode != MeasureSpec.EXACTLY) {
			height = width
		}
		if (widthMode == MeasureSpec.AT_MOST && width > widthSize) {
			width = widthSize
		}
		if (heightMode == MeasureSpec.AT_MOST && height > heightSize) {
			height = heightSize
		}

		// Ensure the board is square
		height = min(height, width)
		width = height
		cellWidth = (width - paddingLeft - paddingRight) / 9.0f
		cellHeight = (height - paddingTop - paddingBottom) / 9.0f
		setMeasuredDimension(width, height)
		val cellTextSize = cellHeight * 0.75f
		value.textSize = cellTextSize
		textReadOnly.textSize = cellTextSize
		textInvalid.textSize = cellTextSize
		textEvenCells.textSize = cellTextSize
		textTouched.textSize = cellTextSize
		textFocused.textSize = cellTextSize
		textHighlighted.textSize = cellTextSize

		// compute offsets in each cell to center the rendered number
		numberLeft = ((cellWidth - value.measureText("9")) / 2).toInt()
		numberTop = ((cellHeight - value.textSize) / 2).toInt()

		// add some offset because in some resolutions marks are cut-off in the top
		marksTop = cellHeight / 50.0f
		val marksTextSize = (cellHeight - marksTop * 2) / 3.0f
		textMark.textSize = marksTextSize
		textEvenCellsMarks.textSize = marksTextSize
		textMarkFocused.textSize = marksTextSize
		textTouchedMarks.textSize = marksTextSize
		textMarkHighlighted.textSize = marksTextSize
		sectorLineWidth = computeSectorLineWidth(width, height)
		selectedCellFrame.style = Paint.Style.STROKE
		selectedCellFrame.strokeWidth = sectorLineWidth.toFloat()
	}

	private fun computeSectorLineWidth(widthInPx: Int, heightInPx: Int): Int {
		val sizeInPx = min(widthInPx, heightInPx)
		val dipScale = context.resources.displayMetrics.density
		val sizeInDip = sizeInPx / dipScale
		var sectorLineWidthInDip = 2.0f
		if (sizeInDip > 150) {
			sectorLineWidthInDip = 3.0f
		}
		return (sectorLineWidthInDip * dipScale).toInt()
	}

	override fun onTouchEvent(event: MotionEvent): Boolean {
		if (isReadOnlyPreview) return false

		val x = event.x.toInt()
		val y = event.y.toInt()
		when (event.action) {
			MotionEvent.ACTION_DOWN, MotionEvent.ACTION_MOVE -> touchedCell = getCellAtPoint(x, y)
			MotionEvent.ACTION_UP -> {
				selectedCell = getCellAtPoint(x, y)
				performClick()
			}

			MotionEvent.ACTION_CANCEL -> touchedCell = null
		}
		postInvalidate()
		return true
	}

	override fun performClick(): Boolean {
		invalidate() // selected cell has changed, update board as soon as you can
		selectedCell?.let(onCellTappedListener)
		if (autoHideTouchedCellHint) {
			touchedCell = null
		}
		return super.performClick()
	}

	override fun onKeyDown(keyCode: Int, event: KeyEvent): Boolean {
		if (isReadOnlyPreview) return false

		val selectedCell = selectedCell

		when (keyCode) {
			KeyEvent.KEYCODE_DPAD_UP -> return moveCellSelection(0, -1)
			KeyEvent.KEYCODE_DPAD_RIGHT -> return moveCellSelection(1, 0)
			KeyEvent.KEYCODE_DPAD_DOWN -> return moveCellSelection(0, 1)
			KeyEvent.KEYCODE_DPAD_LEFT -> return moveCellSelection(-1, 0)
			KeyEvent.KEYCODE_0, KeyEvent.KEYCODE_SPACE, KeyEvent.KEYCODE_DEL -> {
				// clear value in selected cell
				if (selectedCell != null) {
					if (event.isAltPressed) {
						game.setCellCentralMarks(selectedCell, CellMarks.EMPTY, true)
					} else if (event.isShiftPressed) {
						game.setCellCornerMarks(selectedCell, CellMarks.EMPTY, true)
					} else {
						game.setCellValue(selectedCell, 0, true)
						highlightedValue = 0
						if (moveCellSelectionOnPress) moveCellSelectionRight()
					}
				}
				return true
			}

			KeyEvent.KEYCODE_DPAD_CENTER -> {
				selectedCell?.let(onCellTappedListener)
				return true
			}
		}

		if (keyCode >= KeyEvent.KEYCODE_1 && keyCode <= KeyEvent.KEYCODE_9 && selectedCell != null) {
			val selNumber = keyCode - KeyEvent.KEYCODE_0
			if (event.isAltPressed) {    // add or remove number in cell's central marks
				game.setCellCentralMarks(selectedCell, selectedCell.centralMarks.toggleNumber(selNumber), true)
			} else if (event.isShiftPressed) {    // add or remove number in cell's corner marks
				game.setCellCornerMarks(selectedCell, selectedCell.cornerMarks.toggleNumber(selNumber), true)
			} else {  // enter number in cell
				game.setCellValue(selectedCell, selNumber, true)
				highlightedValue = selNumber
				if (moveCellSelectionOnPress) moveCellSelectionRight()
			}
			return true
		}

		return false
	}

	/**
	 * Moves selected cell by one cell to the right. If edge is reached, selection
	 * skips on beginning of another line.
	 */
	fun moveCellSelectionRight() {
		val selectedCell = selectedCell ?: return

		if (!moveCellSelection(1, 0)) {
			var selRow = selectedCell.rowIndex
			selRow++
			if (!moveCellSelectionTo(selRow, 0)) {
				moveCellSelectionTo(0, 0)
			}
		}
		postInvalidate()
	}

	/**
	 * Moves selected by vx cells right and vy cells down. vx and vy can be negative. Returns true,
	 * if new cell is selected.
	 *
	 * @param vx Horizontal offset, by which move selected cell.
	 * @param vy Vertical offset, by which move selected cell.
	 */
	private fun moveCellSelection(vx: Int, vy: Int): Boolean {
		val selectedCell = selectedCell ?: return false
		val newRow = selectedCell.rowIndex + vy
		val newCol = selectedCell.columnIndex + vx
		return moveCellSelectionTo(newRow, newCol)
	}

	/**
	 * Moves selection to the cell given by row and column index.
	 *
	 * @param row Row index of cell which should be selected.
	 * @param col Column index of cell which should be selected.
	 * @return True, if cell was successfully selected.
	 */
	fun moveCellSelectionTo(row: Int, col: Int): Boolean {
		if (col >= 0 && col < SudokuBoard.SUDOKU_SIZE && row >= 0 && row < SudokuBoard.SUDOKU_SIZE) {
			selectedCell = board.getCell(row, col)
			postInvalidate()
			return true
		}
		return false
	}

	fun clearCellSelection() {
		selectedCell = null
		postInvalidate()
	}

	/**
	 * Returns cell at given screen coordinates. Returns null if no cell is found.
	 */
	private fun getCellAtPoint(x: Int, y: Int): Cell? {
		// take into account padding
		val lx = x - paddingLeft
		val ly = y - paddingTop
		val row = (ly / cellHeight).toInt()
		val col = (lx / cellWidth).toInt()
		return if (col >= 0 && col < SudokuBoard.SUDOKU_SIZE && row >= 0 && row < SudokuBoard.SUDOKU_SIZE) {
			board.getCell(row, col)
		} else {
			null
		}
	}

	fun blinkValue(digit: Int) {
		val anim: Animation = AlphaAnimation(0.0f, 1.0f)
		anim.duration = 50 //You can manage the blinking time with this parameter
		anim.startOffset = 20
		anim.repeatMode = Animation.REVERSE
		anim.repeatCount = 4
		blinker.blink(digit)
	}

	companion object {
		const val DEFAULT_BOARD_SIZE = 100

		private class Blinker(private val sudokuBoard: SudokuBoardView) : Timer(250) {
			private var blinkCount = 0
			private var blinkingDigit = 0

			fun blink(newDigit: Int) {
				blinkingDigit = newDigit
				blinkCount = 0
				start()
			}

			override fun step(): Boolean {
				if (sudokuBoard.blinkingDigit == 0) {
					sudokuBoard.blinkingDigit = blinkingDigit
					blinkCount += 1
				} else {
					sudokuBoard.blinkingDigit = 0
					if (blinkCount >= 2) { // stop blinking
						sudokuBoard.postInvalidate()
						return true
					}
				}
				sudokuBoard.postInvalidate()
				return false
			}
		}
	}
}

enum class MarkPlacement(val textAlign: Paint.Align, val xm: Float, val ym: Float) {
	CENTER(Paint.Align.CENTER, 0.5f, 0.5f),
	TOP_LEFT(Paint.Align.LEFT, 0.05f, 0.06f),
	TOP_RIGHT(Paint.Align.RIGHT, 0.97f, 0.06f),
	BOTTOM_LEFT(Paint.Align.LEFT, 0.05f, 0.94f),
	BOTTOM_RIGHT(Paint.Align.RIGHT, 0.97f, 0.94f),
}

internal fun Paint.getCharWidth(singleCharString: String): Float {
	val widths = FloatArray(1)
	getTextWidths(singleCharString, widths)
	return widths[0]
}
