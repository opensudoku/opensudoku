/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import org.moire.opensudoku.db.Column

// Intent data names and XML tags for export and import
object Tag {
	const val FOLDER = "folder"
	const val GAME = "game"
	const val PUZZLE_ID = "puzzle_id"
	const val CELL_COLLECTION = "cell_collection"
	const val FOLDER_ID = Column.FOLDER_ID
	const val CREATED = Column.CREATED
	const val STATE = Column.STATE
	const val MISTAKE_COUNTER = Column.MISTAKE_COUNTER
	const val TIME = Column.TIME
	const val LAST_PLAYED = Column.LAST_PLAYED
	const val CELLS_DATA = Column.CELLS_DATA
	const val USER_NOTE = Column.USER_NOTE
	const val COMMAND_STACK = Column.COMMAND_STACK
	const val NAME = Column.NAME
	const val IMPORT_STRATEGY = "import_strategy"
}
