/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import com.google.android.gms.tasks.Task
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.text.Text
import com.google.mlkit.vision.text.TextRecognition
import com.google.mlkit.vision.text.TextRecognizer
import com.google.mlkit.vision.text.latin.TextRecognizerOptions
import org.moire.opensudoku.R
import org.moire.opensudoku.game.SudokuBoard
import org.moire.opensudoku.game.SudokuBoard.Companion.SUDOKU_SIZE
import org.moire.opensudoku.game.SudokuBoard.Companion.fromArray
import org.moire.opensudoku.game.SudokuSolver
import org.opencv.android.CameraActivity
import org.opencv.android.CameraBridgeViewBase
import org.opencv.android.CameraBridgeViewBase.CvCameraViewFrame
import org.opencv.android.CameraBridgeViewBase.CvCameraViewListener2
import org.opencv.android.OpenCVLoader
import org.opencv.android.Utils
import org.opencv.core.Core
import org.opencv.core.CvType
import org.opencv.core.Mat
import org.opencv.core.MatOfPoint
import org.opencv.core.MatOfPoint2f
import org.opencv.core.Point
import org.opencv.core.Scalar
import org.opencv.core.Size
import org.opencv.imgproc.Imgproc
import kotlin.math.ceil
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min
import kotlin.math.pow
import kotlin.math.sqrt

// Number of required successful recognitions
const val NUMBER_OF_REQUIRED_VALID_PUZZLE_RECOGNITIONS = 3

/**
 * Uses the device's camera to try and recognise a sudoku. The returned `Intent` will have a String extra that contains the details of the recognized board.
 *
 * Board recognition can be inaccurate. To guard against this, a recognized board must:
 *  * be valid (>= 17 digits, no repeats in rows, columns, or boxes)
 *  * have unique solution
 *  * have more digits than any board from a previous frame
 *
 * Recognition attempts run repeatedly. The board must be recognized `NUMBER_OF_REQUIRED_VALID_PUZZLE_RECOGNITIONS` times (in total, not in sequence) before
 * it is returned to the calling activity.
 */
class SudokuScanActivity : CameraActivity(), CvCameraViewListener2 {
	private lateinit var openCvCameraView: CameraBridgeViewBase

	// Text recognition engine
	private lateinit var textRecognizer: TextRecognizer

	// The original camera image
	private lateinit var imageRGBA: Mat

	// Grey scale version of the camera image
	private lateinit var imageRGBAG: Mat

	// An image to overlay over the video feed, highlighting the centre of the screen
	private lateinit var imageViewFinder: Mat

	// Warped / transformed version of the image to analyse
	private var imageCorrected: Mat? = null

	// Edge contours discovered in the camera image
	private val contours: MutableList<MatOfPoint> = ArrayList()

	// True if image analysis is in progress
	private var isDuringOCR = false

	// The last board recognized (can be invalid)
	private var lastRecognizedBoard: Array<IntArray>? = null

	// Successful recognition counter
	private var successfulRecognitionCount = 0

	// Best recognized CellCollection so far
	private var bestCollection: SudokuBoard? = null

	// Debug counters
	private var ocrCount = 0
	private var frameCount = 0

	public override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		if (!OpenCVLoader.initLocal()) {
			Log.e(TAG, "OpenCV initialization failed!")
			Toast.makeText(this, "OpenCV initialization failed!", Toast.LENGTH_LONG).show()
			finish()
			return
		}

		this.window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
		this.setContentView(R.layout.puzzle_scan)

		openCvCameraView = findViewById<CameraBridgeViewBase>(R.id.camera_view).apply {
			visibility = View.VISIBLE
			setCvCameraViewListener(this@SudokuScanActivity)
		}
		textRecognizer = TextRecognition.getClient(TextRecognizerOptions.DEFAULT_OPTIONS)
	}

	public override fun onPause() {
		super.onPause()
		openCvCameraView.disableView()
	}

	public override fun onResume() {
		super.onResume()
		openCvCameraView.enableView()
	}

	override fun getCameraViewList(): List<CameraBridgeViewBase> {
		return listOf(openCvCameraView)
	}

	public override fun onDestroy() {
		super.onDestroy()
		openCvCameraView.disableView()
	}

	override fun onCameraViewStarted(width: Int, height: Int) {
		imageRGBA = Mat(height, width, CvType.CV_8UC4)
		imageRGBAG = Mat(height, width, CvType.CV_8UC4)

		// Create viewfinder overlay. Viewfinder leaves a rectangle that is 80% of the image height (i.e., a 10% margin top/bottom) in the middle of the screen,
		// everything outside the square is darkened.
		// The viewfinder dimensions aren't used when searching for the Sudoku board on the image, it's just an aid to the user to maximise the chance
		// of getting a clean scan.
		imageViewFinder = Mat.zeros(imageRGBA.size(), imageRGBA.type())
		val margin = height * 0.1
		val topLeft = Point(width / 2.0 - height / 2.0, margin)
		val bottomRight = Point(width / 2.0 + height / 2.0, height - margin)
		Imgproc.rectangle(imageViewFinder, topLeft, bottomRight, WHITE, Imgproc.FILLED)
	}

	override fun onCameraViewStopped() {
		imageRGBA.release()
		imageRGBAG.release()
		imageViewFinder.release()
		imageCorrected?.release()
	}

	override fun onCameraFrame(inputFrame: CvCameraViewFrame): Mat {
		frameCount += 1
		imageRGBA = inputFrame.rgba()

		// Don't analyse the frame if we're still analysing a prior frame
		if (isDuringOCR) {
			compositeViewFinder(imageRGBA, imageViewFinder)
			compositeDebugInfo()
			return imageRGBA
		}
		isDuringOCR = true

		// Find the board
		imageRGBAG = inputFrame.gray()
		val boardContour = findSudokuBoardContour(imageRGBAG)
		if (boardContour == null) {
			compositeViewFinder(imageRGBA, imageViewFinder)
			compositeDebugInfo()
			isDuringOCR = false
			return imageRGBA
		}

		// Draw the detected board contour as a green line around the board. As more successful scans are made the line gets thicker and greener.
		val pts = MatOfPoint()
		boardContour.convertTo(pts, CvType.CV_32S)
		Imgproc.drawContours(imageRGBA, listOf(pts), 0, DARK_GREEN, 3 * 2)

		// Extract the sudoku grid from the grey-scale image and remove the perspective so it appears planar.
		val imageToCorrect = imageRGBAG.clone()
		removePerspective(imageRGBAG, imageToCorrect, boardContour)

		// If analysis found something other than a roughly-square grid that's at least 100px by 100px then the corrected image will be too small, and/or have
		// odd dimensions. No sense in analysing it further.
		val grid = imageToCorrect.getGridInfo()
		if (grid.gridWidth < 100 || grid.gridHeight < 100) {
			Log.d(TAG, "Skip frame, grid image is too small ${grid.gridWidth} x ${grid.gridHeight}")
			compositeViewFinder(imageRGBA, imageViewFinder)
			compositeDebugInfo()
			isDuringOCR = false
			return imageRGBA
		}

		if (grid.gridWidth < grid.gridHeight * 0.85 || grid.gridWidth > grid.gridHeight * 1.15) {
			Log.d(TAG, "Skip frame, corrected image is too far from square: ${grid.gridWidth} x ${grid.gridHeight}")
			compositeViewFinder(imageRGBA, imageViewFinder)
			compositeDebugInfo()
			isDuringOCR = false
			return imageRGBA
		}

		// Threshold and invert the image ahead of further analysis
		Imgproc.threshold(imageToCorrect, imageToCorrect, 0.0, 255.0, Imgproc.THRESH_BINARY_INV + Imgproc.THRESH_OTSU)

		/*
		// The following finding lines seems to be less effective than just removing the lines where they're expected to be. Let's keep the code though for
		// some time just in case it will still be useful (2024-04-13)

		// Identify and remove the grid lines
		val lines = Mat()
		val horizontalLines: List<MatOfPoint> = ArrayList()
		val verticalLines: List<MatOfPoint> = ArrayList()

		// Find the horizontal lines
		val hKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(50.0, 1.0))
		Imgproc.morphologyEx(imageCorrected, lines, Imgproc.MORPH_OPEN, hKernel)
		Imgproc.findContours(lines, horizontalLines, Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)

		// Find the vertical lines
		val vKernel = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, Size(1.0, 40.0))
		Imgproc.morphologyEx(imageCorrected, lines, Imgproc.MORPH_OPEN, vKernel)
		Imgproc.findContours(lines, verticalLines, Mat(), Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE)

		// Remove the found lines. This seems more effective than first removing the horizontal lines and then trying to detect the vertical lines, as removing
		// the horizontal lines before finding the vertical lines leaves gaps in the image.
		Imgproc.drawContours(imageCorrected, horizontalLines, -1, BLACK, 20)
		Imgproc.drawContours(imageCorrected, verticalLines, -1, BLACK, 20)
		 */

		imageToCorrect.drawGrid(SUDOKU_SIZE, SUDOKU_SIZE, BLACK, 70)

		// Build the image for text recognition
		val bitmap = Bitmap.createBitmap(imageToCorrect.width(), imageToCorrect.height(), Bitmap.Config.ARGB_8888)
		Utils.matToBitmap(imageToCorrect, bitmap)
		val inputImage = InputImage.fromBitmap(bitmap, 0)

		// Run the text recognizer
		textRecognizer.process(inputImage).addOnCompleteListener(::deferredAnalyzeOcrResult)
		compositeViewFinder(imageRGBA, imageViewFinder)
		imageCorrected = imageToCorrect
		compositeDebugInfo()
		return imageRGBA
	}

	private fun deferredAnalyzeOcrResult(ocrTask: Task<Text>) {
		try {
			analyzeOcrResult(ocrTask)
		} finally {
			isDuringOCR = false
		}
	}

	private fun analyzeOcrResult(ocrTask: Task<Text>) {
		if (!ocrTask.isSuccessful) {
			return
		}

		ocrCount += 1
		val text = ocrTask.result
		val values = Array(SUDOKU_SIZE) { IntArray(SUDOKU_SIZE) }
		var numbersRecognized = 0
		val grid = imageCorrected!!.getGridInfo()

		for (block in text.textBlocks) {
			for (line in block.lines) {
				for (element in line.elements) {
					val rect = element.boundingBox
					if (rect == null) {
						Log.d(TAG, "Skip element, null rect")
						continue
					}

					// Determine which cell the recognized number is in from the center of its bounding rectangle (corrected for padding)
					var (row, col) = grid.getRowColForXY(rect.centerX(), rect.centerY())
					if (row < 0 || row > 8) {
						Log.d(TAG, "Skip element, row = $row")
						continue
					}
					if (col < 0 || col > 8) {
						Log.d(TAG, "Skip element, col = $col")
						continue
					}

					// Work around an issue with MLKit -- very occasionally it seems to return the same element twice, which throws off
					// the count of numbersRecognized. Skip this element if the board already contains a number here.
					if (values[row][col] != 0) {
						Log.d(TAG, "Skip element, seen entry at row=$row, col=$col")
						continue
					}
					val numChars = element.text

					// May have detected multiple numbers in a single element. For example, the two cells next to each other "6 | 8" may
					// be detected as a single text, "68". If so, process each character separately.
					for (i in numChars.indices) {
						val c = autoConvertToDigits(element.text[i])
						val number = c.code - '0'.code
						if (number < 1 || number > 9) {
							Log.d(TAG, "Skip character out of range: '$c' -> $number")
							continue
						}
						numbersRecognized++
						values[row][col] = number
						col += 1
						if (col >= SUDOKU_SIZE) {
							break
						}
					}
				}
			}
		}

		lastRecognizedBoard = values

		// A solvable Sudoku board must have at least 17 given digits.
		if (numbersRecognized < 17) {
			Log.d(TAG, "Skip frame, numbersRecognized=$numbersRecognized, < 17")
			return
		}

		// Cells must be in a valid state. An invalid state (e.g., two of the same
		// number in a row, column, or box) indicates failed text recognition.
		val board = fromArray(values, false)
		if (!board.validate()) {
			Log.d(TAG, "Skip frame, invalid board")
			return
		}

		if (SudokuSolver.countSolutions(board, 2) != 1) {
			Log.d(TAG, "Skip frame, not a valid puzzle")
			return
		}

		// Valid board with a unique solution has been recognised.
		Log.d(TAG, "Valid board recognized")
		successfulRecognitionCount += 1
		if (board.valuesCount > (bestCollection?.valuesCount ?: 0)) {
			bestCollection = board
		}
		if (successfulRecognitionCount >= NUMBER_OF_REQUIRED_VALID_PUZZLE_RECOGNITIONS) {
			this.setResult(RESULT_OK, Intent().putExtra(Tag.CELL_COLLECTION, bestCollection!!.serialize()))
			finish()
		}
	}

	/**
	 * Composites the viewfinder over the frame. The viewfinder shows where to aim, and information about the number of scans still to do.
	 *
	 * @param background the image to composite over
	 * @param viewFinder an image to composite over `background`
	 */
	private fun compositeViewFinder(background: Mat, viewFinder: Mat) {
		Core.addWeighted(background, 0.5, viewFinder, 0.3, 0.0, background)

		// Marker starts 5% of the way down the image
		val y = (background.height() * 0.05).toInt()

		// Center-align the markers
		val markerSize = 40
		var x = background.width() / 2 - (NUMBER_OF_REQUIRED_VALID_PUZZLE_RECOGNITIONS - 1) * markerSize * 2 / 2
		var i = 1
		while (i <= NUMBER_OF_REQUIRED_VALID_PUZZLE_RECOGNITIONS) {
			val color = if (i < successfulRecognitionCount) GREEN else RED
			Imgproc.drawMarker(background, Point(x.toDouble(), y.toDouble()), color, Imgproc.MARKER_SQUARE, markerSize)
			x += markerSize * 2
			i++
		}
	}

	/**
	 * Generates and composites debug information over the frame.
	 */
	private fun compositeDebugInfo() {
		if (SHOW_ADDITIONAL_DEBUG_COUNTERS_INFO) {
			@SuppressLint("DefaultLocale") val text = "Debug info enabled, frame count:$frameCount, OCR count: $ocrCount"
			val textPosition = Point(0.0, (imageRGBA.height() - 40).toDouble())
			Imgproc.putText(imageRGBA, text, textPosition, Imgproc.FONT_HERSHEY_DUPLEX, 2.0, RED)
		}

		val imageToShow = imageCorrected?.clone() ?: return

		if (imageToShow.type() == CvType.CV_8UC1) {
			Imgproc.cvtColor(imageToShow, imageToShow, Imgproc.COLOR_GRAY2BGRA)
		}

		imageToShow.drawGrid(SUDOKU_SIZE, SUDOKU_SIZE, RED, 70)

		// The transformation that created debugImage may have made it too wide or tall. Scale it appropriately.
		val maxHeight = imageRGBA.height()
		val maxWidth = imageRGBA.width() / 2
		val scaleX = maxWidth.toDouble() / imageToShow.width()
		val scaleY = maxHeight.toDouble() / imageToShow.height()
		val scale = max(scaleX, scaleY)
		if (scale < 1) {
			Imgproc.resize(imageToShow, imageToShow, Size(imageToShow.width() * scale, imageToShow.height() * scale))
		}
		lastRecognizedBoard?.let { imageToShow.printInValues(it) }

		val sub = imageRGBA.submat(0, imageToShow.rows(), 0, imageToShow.cols())
		Core.addWeighted(sub, 0.0, imageToShow, 1.0, 1.0, sub)

		imageToShow.release()
	}

	/**
	 * Corrects for possible errors in text recognition. MLKit does not support specifying a set of characters that should be recognised
	 * (e.g., only digits), so some digits may be mis-recognized as letters or symbols.
	 *
	 * @param c Non-digit character that has been mis-recognized.
	 * @return The most likely digit equivalent to the c.
	 */
	private fun autoConvertToDigits(c: Char): Char {
		when (c) {
			'|', 'I', 'l' -> return '1'
			'Z' -> return '2'
			'A' -> return '4'
			'S' -> return '5'
			'b' -> return '6'
			'B' -> return '8'
		}
		return c
	}

	/**
	 * Removes the perspective from an image. Assumes that a region of the image that should be (approximately) square has been identified.
	 *
	 * @param srcImage the image to correct
	 * @param dstImage destination for the corrected image
	 * @param contour contour defining a region of `srcImage` that should be approximately square after perspective correction
	 */
	private fun removePerspective(srcImage: Mat?, dstImage: Mat?, contour: MatOfPoint2f) {
		val cornerPoints = findCornerPoints(contour.toArray())
		val topLeft = cornerPoints[0]
		val topRight = cornerPoints[1]
		val bottomRight = cornerPoints[2]
		val bottomLeft = cornerPoints[3]

		// Compute new image width
		val widthA = sqrt((bottomRight.x - bottomLeft.x).pow(2.0) + (bottomRight.y - bottomLeft.y).pow(2.0))
		val widthB = sqrt((topRight.x - topLeft.x).pow(2.0) + (topRight.y - topLeft.y).pow(2.0))
		val maxWidth = min(widthA, widthB)

		// Compute the new image height
		val heightA = sqrt((topRight.x - bottomRight.x).pow(2.0) + (topRight.y - bottomRight.y).pow(2.0))
		val heightB = sqrt((topLeft.x - bottomLeft.x).pow(2.0) + (topLeft.y - bottomLeft.y).pow(2.0))
		val maxHeight = min(heightA, heightB)
		val rect = MatOfPoint2f(*cornerPoints)
		val dst = MatOfPoint2f(
			Point(PERSPECTIVE_CORRECTION_PADDING.toDouble(), PERSPECTIVE_CORRECTION_PADDING.toDouble()),
			Point(maxWidth - PERSPECTIVE_CORRECTION_PADDING, PERSPECTIVE_CORRECTION_PADDING.toDouble()),
			Point(maxWidth - PERSPECTIVE_CORRECTION_PADDING, maxHeight - PERSPECTIVE_CORRECTION_PADDING),
			Point(PERSPECTIVE_CORRECTION_PADDING.toDouble(), maxHeight - PERSPECTIVE_CORRECTION_PADDING)
		)
		val transform = Imgproc.getPerspectiveTransform(rect, dst)
		Imgproc.warpPerspective(srcImage, dstImage, transform, Size(maxWidth, maxHeight))
	}

	/**
	 * Finds the corner points in an array of [Point].
	 *
	 * @param points the points to check
	 * @return array of 4 points, top-left, top-right, bottom-right, bottom-left, in that order.
	 */
	private fun findCornerPoints(points: Array<Point>): Array<Point> {
		/*
		 * To sort the points obtain the sum and differences of each point's X and Y coordinates.
		 * The top-left point has the smallest sum.
		 * The top-right point has the smallest difference.
		 * The bottom-right point has the largest sum.
		 * The bottom-left point has the largest difference.
		 *
		 * Consider this grid (origin is 0/0, top-left), with four points.
		 * 0 1 2 3 4 5 6 7 8 9
		 * 1
		 * 2             *
		 * 3
		 * 4   *
		 * 5                 *
		 * 6
		 * 7       *
		 *
		 * x  y     sum    diff (y-x)
		 * 2  4      6      2          Smallest sum, top-left
		 * 7  2      9     -5          Smallest diff, top-right
		 * 9  5     14     -3          Largest sum, bottom-right
		 * 4  7     11      3          Largest diff, bottom-left
	     */

		var maxSum = Double.MIN_VALUE
		var maxSumIdx = 0
		var minSum = Double.MAX_VALUE
		var minSumIdx = 0
		var maxDiff = Double.MIN_VALUE
		var maxDiffIdx = 0
		var minDiff = Double.MAX_VALUE
		var minDiffIdx = 0
		for (i in points.indices) {
			val sum = points[i].x + points[i].y
			val diff = points[i].y - points[i].x
			if (sum > maxSum) {
				maxSum = sum
				maxSumIdx = i
			}
			if (sum < minSum) {
				minSum = sum
				minSumIdx = i
			}
			if (diff > maxDiff) {
				maxDiff = diff
				maxDiffIdx = i
			}
			if (diff < minDiff) {
				minDiff = diff
				minDiffIdx = i
			}
		}
		return arrayOf(points[minSumIdx], points[minDiffIdx], points[maxSumIdx], points[maxDiffIdx])
	}

	/**
	 * Finds the contour of a sudoku board in an image. The board is assumed to be the contour that has the greatest area.
	 *
	 * @param image image to search.
	 * @return [Mat] that contains the contour of the board in `image`, or `null` if no contours were found.
	 */
	private fun findSudokuBoardContour(image: Mat?): MatOfPoint2f? {
		val edges = Mat()
		Imgproc.Canny(image, edges, 200.0, 255.0)

		// Get the contours. If no contours are detectable then there's no board.
		contours.clear()
		Imgproc.findContours(edges, contours, Mat(), Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE)
		if (contours.isEmpty()) {
			return null
		}

		// Find the contour with the largest area.
		var maxA = 0.0
		var imax = 0
		for (i in contours.indices) {
			val contour: Mat = contours[i]
			if (Imgproc.contourArea(contour) > maxA) {
				maxA = Imgproc.contourArea(contour)
				imax = i
			}
		}

		// Approximate the contour to an equilateral
		val c2f = MatOfPoint2f(*contours[imax].toArray())
		val approx = MatOfPoint2f()
		val perimeter = Imgproc.arcLength(c2f, true)
		Imgproc.approxPolyDP(c2f, approx, 0.01 * perimeter, true)
		return approx
	}

	companion object {
		private val TAG = SudokuScanActivity::class.java.simpleName

		// True if the debug overlay should be shown
		private const val SHOW_ADDITIONAL_DEBUG_COUNTERS_INFO = false

		// Amount of padding (px) to add around the board image during perspective correction
		const val PERSPECTIVE_CORRECTION_PADDING = 10

		// Colors
		val BLACK = Scalar(0.0, 0.0, 0.0)
		val RED = Scalar(255.0, 0.0, 0.0)
		val GREEN = Scalar(0.0, 255.0, 0.0)
		val DARK_GREEN = Scalar(0.0, 127.0, 0.0)
		val WHITE = Scalar(255.0, 255.0, 255.0)
	}
}

internal fun Mat.printInValues(lastRecognizedBoard: Array<IntArray>) {
	val grid = getGridInfo()
	val textLocation = Point()
	lastRecognizedBoard.forEachIndexed { row, cols ->
		cols.forEachIndexed { col, value ->
			if (value != 0) {
				val textSize = Imgproc.getTextSize("$value", Imgproc.FONT_HERSHEY_DUPLEX, 1.5, 2, null)
				textLocation.x = grid.getColX(col + 0.5) - textSize.width / 2
				textLocation.y = grid.getRowY(row + 0.5) + textSize.height / 2
				Imgproc.putText(this, "$value", textLocation, Imgproc.FONT_HERSHEY_DUPLEX, 1.5, SudokuScanActivity.RED, 2)
			}
		}
	}
}

// Draws in rows and columns
internal fun Mat.drawGrid(rows: Int, cols: Int, color: Scalar, thicknessDivisor: Int) {
	val grid = getGridInfo()
	val start = Point()
	val end = Point()

	// horizontal lines
	start.x = grid.minX
	end.x = grid.maxX
	var thickness = ceil(grid.gridHeight / thicknessDivisor).toInt()
	for (row in 0..rows) {
		grid.getRowY(row.toDouble()).let { start.y = it; end.y = it }
		Imgproc.line(this, start, end, color, thickness)
	}

	// vertical lines
	start.y = grid.minY
	end.y = grid.maxY
	thickness = ceil(grid.gridWidth / thicknessDivisor).toInt()
	for (col in 0..cols) {
		grid.getColX(col.toDouble()).let { start.x = it; end.x = it }
		Imgproc.line(this, start, end, color, thickness)
	}
}

internal fun Mat.getGridInfo(): GridParams {
	return GridParams().apply {
		xPadding = width().toDouble() / 60
		yPadding = height().toDouble() / 60
		minX = xPadding
		minY = yPadding
		maxX = (width()).toDouble() - xPadding
		maxY = (height()).toDouble() - yPadding
		gridWidth = maxX - minX
		gridHeight = maxY - minY
		cellWidth = gridWidth / SUDOKU_SIZE
		cellHeight = gridWidth / SUDOKU_SIZE
	}
}

class GridParams {
	var xPadding: Double = 0.0
	var yPadding: Double = 0.0
	var minX: Double = 0.0
	var maxX: Double = 0.0
	var minY: Double = 0.0
	var maxY: Double = 0.0
	var gridHeight: Double = 0.0
	var gridWidth: Double = 0.0
	var cellHeight: Double = 0.0
	var cellWidth: Double = 0.0

	fun getColX(col: Double): Double = minX + gridWidth * col / SUDOKU_SIZE

	fun getRowY(row: Double): Double = minY + gridHeight * row / SUDOKU_SIZE
	fun getRowColForXY(x: Int, y: Int): Pair<Int, Int> {
		val row: Int = floor((y - yPadding) / cellHeight).toInt()
		val col: Int = floor((x - xPadding) / cellWidth).toInt()
		return row to col
	}
}
