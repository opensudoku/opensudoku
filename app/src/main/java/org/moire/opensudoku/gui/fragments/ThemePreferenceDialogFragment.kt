/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui.fragments

import android.graphics.drawable.Drawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.content.res.ResourcesCompat
import androidx.preference.ListPreference
import androidx.preference.ListPreferenceDialogFragmentCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.radiobutton.MaterialRadioButton
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.gui.SudokuBoardView
import org.moire.opensudoku.gui.fragments.ThemePreferenceDialogFragment.ThemeAdapter.ViewHolder
import org.moire.opensudoku.utils.ThemeUtils

/**
 * Dialog fragment that displays a list of themes and a game board to
 * preview the theme.
 *
 * Necessary because although an AlertDialog allows you to display a list of
 * items and specify a custom view, you cannot do both of these things at
 * the same time.
 *
 * This class is a reimplementation of [ListPreferenceDialogFragmentCompat]
 * but without calling setSingleChoiceItems() on the AlertDialog.Builder.
 *
 * This allows the class to set its own view which includes the list view
 * as well as other views.
 *
 * This also means this class has to manage the list adapter and recycler
 * view as well, instead of allowing the AlertDialog to do it.
 */
class ThemePreferenceDialogFragment : ListPreferenceDialogFragmentCompat() {
	private lateinit var boardView: SudokuBoardView
	var clickedDialogEntryIndex = 0
	private lateinit var entries: Array<CharSequence?>
	private lateinit var entryValues: Array<CharSequence>
	private lateinit var adapter: ThemeAdapter

	private val listPreference: ListPreference
		get() = preference as ListPreference

	private val onItemClickListener = View.OnClickListener { v ->
		val viewHolder = v.tag as ViewHolder
		val prevSelectedPosition = clickedDialogEntryIndex
		clickedDialogEntryIndex = viewHolder.adapterPosition
		adapter.notifyItemChanged(prevSelectedPosition)
		adapter.notifyItemChanged(clickedDialogEntryIndex)
		applyThemePreview(entryValues[clickedDialogEntryIndex] as String)
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		if (savedInstanceState == null) {
			val preference = listPreference
			check(!(preference.entries == null || preference.entryValues == null)) {
				@Suppress("HardCodedStringLiteral") "ListPreference requires an entries array and an entryValues array."
			}
			val themeCode = preference.value
			clickedDialogEntryIndex = preference.findIndexOfValue(themeCode)
			entries = preference.entries
			entryValues = preference.entryValues
		} else {
			clickedDialogEntryIndex = savedInstanceState.getInt(SAVE_STATE_INDEX, 0)
			entries = savedInstanceState.getCharSequenceArray(SAVE_STATE_ENTRIES)!!
			entryValues = savedInstanceState.getCharSequenceArray(SAVE_STATE_ENTRY_VALUES)!!
		}
	}

	override fun onSaveInstanceState(outState: Bundle) {
		super.onSaveInstanceState(outState)
		outState.putInt(SAVE_STATE_INDEX, clickedDialogEntryIndex)
		outState.putCharSequenceArray(SAVE_STATE_ENTRIES, entries)
		outState.putCharSequenceArray(SAVE_STATE_ENTRY_VALUES, entryValues)
	}

	override fun onPrepareDialogBuilder(builder: AlertDialog.Builder) {
		val inflater = LayoutInflater.from(context)
		val preferenceView = inflater.inflate(R.layout.preference_dialog_sudoku_board_theme, null)
		boardView = preferenceView.findViewById(R.id.preference_board_view)
		val recyclerView = preferenceView.findViewById<RecyclerView>(R.id.theme_list)
		val layoutManager = LinearLayoutManager(requireContext())
		recyclerView.layoutManager = layoutManager
		layoutManager.scrollToPosition(clickedDialogEntryIndex)
		adapter = ThemeAdapter(entries)
		recyclerView.adapter = adapter
		adapter.onItemClickListener = onItemClickListener
		prepareBoardPreviewView("${entryValues[clickedDialogEntryIndex]}")
		builder.setView(preferenceView)
		builder.setTitle("")
	}

	override fun onDialogClosed(positiveResult: Boolean) {
		if (positiveResult && clickedDialogEntryIndex >= 0) {
			val value = "${entryValues[clickedDialogEntryIndex]}"

			val preference = listPreference
			if (preference.callChangeListener(value)) {
				preference.value = value
			}
		}
	}

	internal inner class ThemeAdapter(private val entries: Array<CharSequence?>) : RecyclerView.Adapter<ViewHolder?>() {
		internal var onItemClickListener: View.OnClickListener? = null

		/** Drawable for icon that indicates this theme enforces dark mode  */
		private val darkModeIcon: Drawable? = ResourcesCompat.getDrawable(resources, R.drawable.ic_baseline_dark_mode, requireContext().theme)

		internal inner class ViewHolder(itemView: MaterialRadioButton) : RecyclerView.ViewHolder(itemView) {
			val radioButton: MaterialRadioButton

			init {
				itemView.tag = this
				itemView.setOnClickListener(onItemClickListener)
				radioButton = itemView.findViewById(android.R.id.text1)
			}
		}

		override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
			val v = LayoutInflater.from(parent.context)
				.inflate(R.layout.preference_dialog_theme_listitem, parent, false) as MaterialRadioButton
			return ViewHolder(v)
		}

		override fun onBindViewHolder(holder: ViewHolder, position: Int) {
			val button = holder.radioButton
			button.text = entries[position]
			button.isChecked = position == clickedDialogEntryIndex
			if (ThemeUtils.isDarkTheme(entryValues[position] as String)) {
				button.setCompoundDrawablesWithIntrinsicBounds(null, null, darkModeIcon, null)
			} else {
				button.setCompoundDrawables(null, null, null, null)
			}
		}

		override fun getItemCount(): Int = entries.size
	}

	private fun prepareBoardPreviewView(initialTheme: String) {
		boardView.onCellSelectedListener = { cell: Cell? -> boardView.highlightedValue = cell?.value ?: 0 }
		ThemeUtils.prepareBoardPreviewView(boardView)
		applyThemePreview(initialTheme)
	}

	private fun applyThemePreview(theme: String) {
		ThemeUtils.applyThemeToSudokuBoardViewFromContext(theme, boardView, requireContext())
	}

	companion object {
		private const val SAVE_STATE_INDEX = "ThemePreferenceDialogFragment.index"
		private const val SAVE_STATE_ENTRIES = "ThemePreferenceDialogFragment.entries"
		private const val SAVE_STATE_ENTRY_VALUES = "ThemePreferenceDialogFragment.entryValues"
		fun newInstance(key: String?): ThemePreferenceDialogFragment {
			val fragment = ThemePreferenceDialogFragment()
			val bundle = Bundle(1)
			bundle.putString(ARG_KEY, key)
			fragment.arguments = bundle
			return fragment
		}
	}
}
