/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard
import kotlin.collections.addAll

/** Strategy: Pointing Pair / Pointing Triple
 */
class NextStepPointingPairTriple(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		return checkForPointingPairTriple()
	}

	/** Strategy: Pointing Pair / Pointing Triple
	 *
	 * Pointing Pair and Pointing Triple
	 *  - Intersection Removal
	 * 	- BOX -> LINE Reduction / Interaction
	 *  - Locked Candidates Type 1
	 *
	 *  Check if an candidate number exits in a box only in one row or column.
	 *  If this is the case, these means that this candidate number will
	 *  block the row or column outside of the box.
	 *  You can delete all candidate with this number from the cells of
	 *  the row or column outside of the box.
	 *
	 * Message:
	 *
	 * 		"Pointing Pair: {8} ➠ (b2,c4) / [ r1c4,r3c4 ]"
	 * 	    " ✎ remove {8} from [ r6c4 ]"
	 * 		"Pointing Triple: {8} ➠ (b2,c4] / [ r1c4,r2c4,r3c4 ]"
	 * 	    " ✎ remove {8} from [ r4c4,r5c4,r6c4 ]"
	 *
	 */
	private fun checkForPointingPairTriple(): Boolean {

		var strategyName = "Pointing Pair/Triple"

		// get all houses from type BOX
		val houseType = HouseTypes.BOX
		val houseCellsArray = board.getHousesSectors()

		// loop over all houses in the house type
		forHouse@ for (houseCells in houseCellsArray) {
			val regionCells = arrayListOf<Cell>()
			regionCells.addAll(houseCells.cells)

			// loop over all cells in the house and create a number-cell-map
			var numberCellsMap = mutableMapOf<Int, MutableList<Cell>>()
			forCellInHouse@ for (cell in houseCells.cells) {
				if (cell.value > 0) continue // already solved
				val marksValues = cell.centralMarks.marksValues
				for (number in marksValues) {
					val numberCells =
						numberCellsMap.getOrDefault(number, mutableListOf())
					numberCells.add(cell)
					numberCellsMap[number] = numberCells
				}
			}
			if (numberCellsMap.isEmpty()) continue@forHouse // all cells are solved
			// filter list by count < 4 and sort to make it easier for the user ...
			numberCellsMap = numberCellsMap
				.filter { it.value.size in listOf(2, 3) } as MutableMap<Int, MutableList<Cell>>
			if (numberCellsMap.isEmpty()) continue@forHouse // no cells found
			numberCellsMap = numberCellsMap
				.toSortedMap()
				.toList()
				.sortedBy { (_, value) -> value.size }
				.toMap() as MutableMap<Int, MutableList<Cell>>

			// loop over number sorted by count (single,pair,triple)
			for ((number, cells) in numberCellsMap) {
				strategyName = when (cells.size) {
					2 -> context.getString(R.string.hint_strategy_pointing_pair)
					3 -> context.getString(R.string.hint_strategy_pointing_triple)
					else-> "Pointing Group with size ${cells.size}"
				}
				// all cells in the same row?
				if (cells.map(Cell::rowIndex).toSet().size == 1) {
					val cellsToWorkOn = cells[0].row!!.cells
						.filter { it !in cells }
						.filter { it.value == 0 }
						.filter { it.centralMarks.hasNumber(number) }
					if (cellsToWorkOn.isNotEmpty()) {cells.size
						regionCells.addAll(cells[0].row!!.cells)
						val msgValues = "{$number}"
						val msgCells = "(${houseType.houseAdr(cells[0])},${HouseTypes.ROW.houseAdr(cells[0])}) / ${getCellsGridAddress(cells)}"
						when (hintLevel) {
							HintLevels.LEVEL1 -> { nextStepText = strategyName }
							HintLevels.LEVEL2 -> { nextStepText = "$strategyName: $msgValues" }
							HintLevels.LEVEL3 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells"
								cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
							}
							HintLevels.LEVEL4 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells\n"
								nextStepText += context.getString(R.string.hint_remove_candidate_from,"{$number}",getCellsGridAddress(cellsToWorkOn))
								cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.TARGET] = cellsToWorkOn.map { it.rowIndex to it.columnIndex }
							}
						}
						return true
					}
				}
				// all cells in the same column?
				if (cells.map(Cell::columnIndex).toSet().size == 1) {
					val cellsToWorkOn = cells[0].column!!.cells
						.filter { it !in cells }
						.filter { it.value == 0 }
						.filter { it.centralMarks.hasNumber(number) }
					if (cellsToWorkOn.isNotEmpty()) {
						regionCells.addAll(cells[0].column!!.cells)
						val msgValues = "{$number}"
						val msgCells = "(${houseType.houseAdr(cells[0])},${HouseTypes.COL.houseAdr(cells[0])}) / ${getCellsGridAddress(cells)}"
						when (hintLevel) {
							HintLevels.LEVEL1 -> { nextStepText = strategyName }
							HintLevels.LEVEL2 -> { nextStepText = "$strategyName: $msgValues" }
							HintLevels.LEVEL3 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells"
								cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
							}
							HintLevels.LEVEL4 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells\n"
								nextStepText += context.getString(R.string.hint_remove_candidate_from,"{$number}",getCellsGridAddress(cellsToWorkOn))
								cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.TARGET] = cellsToWorkOn.map { it.rowIndex to it.columnIndex }
							}
						}
						return true
					}
				}
			}
		}
		return false
	}

}

