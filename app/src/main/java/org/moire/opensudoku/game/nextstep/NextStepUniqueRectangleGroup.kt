/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

//TODO: remove translation block unique rectangle text resources

/** Strategy: Unique Rectangle Group
 *
 * The strategies in the group "Unique Rectangle" (UR) are based on four cells.
 * These cells are located in exactly two row, two columns and two blocks.
 * All four cells have the same two candidates. (pure BVC/UR-Cells with UR-Candidates/UR-Pair)
 *
 * 			AB - AB
 * 		    |    |
 * 		    AB - AB
 *
 * This situation will lead into a puzzle with more than on solutions. (deadly pattern)
 * Due too the fact that we are dealing with puzzles with only one solution, we can use this
 * condition as a base for the strategies in this group.
 *
 * For all strategies there must be at least ONE PURE BVC of the four UR-Cells with only two
 * candidates (UR-Pair/UR-Candidates).
 *
 *  the strategy package 'Unique Rectangle Group' contains the following strategies:
 *  - Unique Rectangle Type 1
 *  - ...
 *
 */
open class NextStepUniqueRectangleGroup(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		error("Methode 'search()' is not in use in this subclass 'NextStepUniqueRectangleGroup'!")
		return false
	}

	/** Strategy: Unique Rectangle Type 1
	 *
	 * In this strategy "Unique Rectangle Type 1" only one cell has more than two candidates.
	 *
	 * 			AB - AB
	 * 		    |    |
	 * 		    AB - ABx...
	 *
	 *  This cell can be in one of the four corners.
	 *  If this cell (Unique Corner) has only one additional candidate, this candidate is the
	 *  solution for this cell.
	 *  IF this cell (Unique Corner) has more than one additional candidate, the two UR candidates
	 *  can be removed from this cell. The result will be that the cell has only the additional candidates.
	 *
	 * Messages:
	 *
	 *  	"Unique Rectangle Type 1:"
	 *  	" ⊞ UR-Pair ➠ {1,2}"
	 *  	" ⊞ UR-Cells ➠ [ r1c1,r1c1,r1c1,r1c1 ]"
	 *  	" ⊞ Extra-Candidate ➠ {3}"
	 * 		" ✎ enter value {3} into [ r1c1 ]"
	 *
	 *  	"Unique Rectangle Type 1:"
	 *  	" ⊞ UR-Pair ➠ {1,2}"
	 *  	" ⊞ UR-Cells ➠ [ r1c1,r1c1,r1c1,r1c1 ]"
	 *  	" ⊞ Extra-Candidates ➠ {3,4}"
	 * 		" ✎ remove candidates {1,2} from [ r1c1 ]"
	 *
	 */
	protected fun checkForUniqueRectangle1(): Boolean {

		var strategyName = context.getString(R.string.hint_strategy_unique_rectangle_type_n,"1")

		// loop over all cells to get the first pure Bi-Value-Cell (BVC)
		for ( row in board.getHousesRows()) {
			forCell@ for ( cell in row.cells) {
				if ( cell.value != 0 ) continue@forCell // only empty cells
				if ( cell.centralMarks.marksValues.size != 2 ) continue@forCell // must have two candidates

				// search for the next three cells

				var urCells = mutableListOf<Cell>(cell)
				val ab = urCells[0].centralMarks.marksValues
				var abxFound = false
				var cornerCell = Cell()

				// search the second cell in the row of the first cell
				var cells = urCells[0].row!!.cells
					.filter { it.value == 0 }
					.filter { it != urCells[0] }
					.filter { it.centralMarks.marksValues.containsAll(ab)}
					.filter { it.centralMarks.marksValues.size > 1}
				if ( cells.size != 1 ) continue@forCell // only one cell needed
				if ( cells.first().centralMarks.marksValues.size > 2 )
				{
					if ( abxFound ) continue@forCell
					abxFound = true
					cornerCell = cells.first()
				}
				urCells.add(cells.first())

				// search the third cell in the column of the second cell
				cells = urCells[1].column!!.cells
					.filter { it.value == 0 }
					.filter { it != urCells[1] }
					.filter { it.centralMarks.marksValues.containsAll(ab)}
					.filter { it.centralMarks.marksValues.size > 1}
				if ( cells.size != 1 ) continue@forCell // only one cell needed
				if ( cells.first().centralMarks.marksValues.size > 2 )
				{
					if ( abxFound ) continue@forCell
					abxFound = true
					cornerCell = cells.first()
				}
				urCells.add(cells.first())

				// search the fourth cell in the row of the third cell
				cells = urCells[2].row!!.cells
					.filter { it.value == 0 }
					.filter { it != urCells[2] }
					.filter { it.centralMarks.marksValues.containsAll(ab)}
					.filter { it.centralMarks.marksValues.size > 1}
				if ( cells.size != 1 ) continue@forCell // only one cell needed
				if ( cells.first().centralMarks.marksValues.size > 2 )
				{
					if ( abxFound ) continue@forCell
					abxFound = true
					cornerCell = cells.first()
				}
				urCells.add(cells.first())

				// search the first cell in the column of the fourth cell only to check rectangle
				cells = urCells[3].column!!.cells
					.filter { it.value == 0 }
					.filter { it != urCells[3] }
					.filter { it.centralMarks.marksValues.containsAll(ab)}
					.filter { it.centralMarks.marksValues.size > 1}
				if ( cells.size != 1 ) continue@forCell // only one cell needed

				if ( cells.first() != urCells[0] ) continue@forCell

				// check if UR cells are in two boxes
				val boxes = urCells.map { it.sectorIndex }.toSet().toList()
				if ( boxes.size != 2 ) continue@forCell

				// UR Type 1 found

				val extraCandidates = cornerCell.centralMarks.marksValues.filter { it !in ab }
				val regionCells = mutableListOf<Cell>()
				regionCells.addAll(urCells[0].row!!.cells)
				regionCells.addAll(urCells[1].column!!.cells)
				regionCells.addAll(urCells[2].row!!.cells)
				regionCells.addAll(urCells[3].column!!.cells)

				when (hintLevel) {
					HintLevels.LEVEL1 -> {
						nextStepText = strategyName
					}
					HintLevels.LEVEL2 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair,formatCellMarks(ab)) + "\n"
					}
					HintLevels.LEVEL3 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair,formatCellMarks(ab)) + "\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_cells,getCellsGridAddress(urCells)) + "\n"
						nextStepText += if ( extraCandidates.size == 1)
							context.getString(R.string.hint_strategy_unique_rectangle_candidate,
								formatCellMarks(extraCandidates),"[${cornerCell.gridAddress}]") + "\n"
						else
							context.getString(R.string.hint_strategy_unique_rectangle_candidates,
								formatCellMarks(extraCandidates),"[${cornerCell.gridAddress}]") + "\n"
						cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = urCells.map { it.rowIndex to it.columnIndex }
					}
					HintLevels.LEVEL4 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair,formatCellMarks(ab)) + "\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_cells,getCellsGridAddress(urCells)) + "\n"
						nextStepText += if ( extraCandidates.size == 1)
							context.getString(R.string.hint_strategy_unique_rectangle_candidate,
								formatCellMarks(extraCandidates),"[${cornerCell.gridAddress}]") + "\n"
						else
							context.getString(R.string.hint_strategy_unique_rectangle_candidates,
								formatCellMarks(extraCandidates),"[${cornerCell.gridAddress}]") + "\n"
						nextStepText += if ( extraCandidates.size == 1)
							context.getString(R.string.hint_enter_value_into,
								formatCellMarks(extraCandidates),"[${cornerCell.gridAddress}]") + "\n"
						else
							context.getString(R.string.hint_remove_candidates_from,
								formatCellMarks(ab),"[${cornerCell.gridAddress}]") + "\n"
						cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = urCells.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.TARGET] = listOf(cornerCell).map { it.rowIndex to it.columnIndex }
					}
				}
				return true
			}
		}
		return false
	}

}
