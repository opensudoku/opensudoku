/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2025-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import androidx.preference.PreferenceManager
import org.moire.opensudoku.gui.PuzzleListSorter
import org.moire.opensudoku.gui.Tag
import org.moire.opensudoku.gui.importing.ImportStrategyTypes
import org.moire.opensudoku.utils.CustomTheme
import androidx.core.content.edit
import org.moire.opensudoku.gui.inputmethod.InputMethod

@Suppress("HardCodedStringLiteral")

const val MOST_RECENTLY_PLAYED_PUZZLE_ID = "most_recently_played_puzzle_id"

private const val DOUBLE_MARKS_ENABLED = "double_marks_enabled"
private const val HIGHLIGHT_WRONG_VALUES = "highlight_wrong_values_mode"
private const val HIGHLIGHT_TOUCHED_CELL = "highlight_touched_cell"
private const val HIGHLIGHT_COMPLETED_VALUES = "highlight_completed_values"
private const val SHOW_NUMBER_TOTALS = "show_number_totals"
private const val FILTER_STATE_NOT_STARTED = "filter" + SudokuGame.GAME_STATE_NOT_STARTED
private const val FILTER_STATE_PLAYING = "filter" + SudokuGame.GAME_STATE_PLAYING
private const val FILTER_STATE_SOLVED = "filter" + SudokuGame.GAME_STATE_COMPLETED
private const val SORT_TYPE = "sort_type"
private const val SORT_ORDER = "sort_order"
private const val SHOW_TIME = "show_time"
private const val SHOW_MISTAKE_COUNTER = "show_mistake_counter"
private const val SHOW_PUZZLE_LISTS_ON_STARTUP = "show_puzzle_lists_on_startup"
private const val SCHEMA_VERSION = "schema_version"
private const val SCREEN_BORDER_SIZE = "screen_border_size"
private const val HIGHLIGHT_SIMILAR_CELLS = "highlight_similar_cells"
private const val FILL_IN_MARKS_ENABLED = "fill_in_marks_enabled"
private const val BELL_ENABLED = "bell_enabled"
private const val REMOVE_MARKS_ON_INPUT = "remove_marks_on_input"
private const val IM_MOVE_RIGHT_ON_INSERT = "im_move_right_on_insert_move_right"
private const val SELECTED_INPUT_MODES = "selected_input_modes"
private const val IM_POPUP = "im_popup"
private const val INSERT_ON_TAP = "insert_on_tap"
private const val BIDIRECTIONAL_SELECTION = "bidirectional_selection"
private const val HIGHLIGHT_SIMILAR = "highlight_similar"
private const val SELECT_ON_TAP = "select_on_tap"
private const val SHOW_HINTS = "show_hints"
private const val THEME = "theme"
private const val OPENSUDOKU = "opensudoku"
private const val HIGHLIGHT_SIMILAR_MARKS_MODE = "highlight_similar_marks_mode"
private const val VALUES = "values"
const val CUSTOM_THEME_UI_MODE = "custom_theme_ui_mode"
private const val SYSTEM = "system"
private const val ACTIVE_INPUT_METHOD_INDEX = "active_input_method_index"
private const val INPUT_METHOD_LAST_GAME_ID = "input_method_last_game_id"
private const val INPUT_METHOD_SELECTED_NUMBER = "input_method_selected_number"
private const val INPUT_METHOD_EDIT_MODE = "input_method_edit_mode"

class GameSettings(applicationContext: Context) {
    private val preferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

    // Int
    var schemaVersion
        get() = preferences.getInt(SCHEMA_VERSION, 20240205)
        set(value) = preferences.edit { putInt(SCHEMA_VERSION, value) }
    val screenBorderSize
        get() = preferences.getInt(SCREEN_BORDER_SIZE, 0)
    var sortType
        get() = preferences.getInt(SORT_TYPE, PuzzleListSorter.SORT_BY_CREATED)
        set(value) = preferences.edit { putInt(SORT_TYPE, value) }
    var colorPrimary
        get() = preferences.getInt(CustomTheme.COLOR_PRIMARY, Color.WHITE)
        set(value) = preferences.edit { putInt(CustomTheme.COLOR_PRIMARY, value) }
    var colorAccent
        get() = preferences.getInt(CustomTheme.COLOR_ACCENT, Color.WHITE)
        set(value) = preferences.edit { putInt(CustomTheme.COLOR_ACCENT, value) }
    var selectedInputMethod
        get() = preferences.getInt(ACTIVE_INPUT_METHOD_INDEX, -1)
        set(value) = preferences.edit { putInt(ACTIVE_INPUT_METHOD_INDEX, value) }
    var keyboardGameId
        get() = preferences.getLong(INPUT_METHOD_LAST_GAME_ID, -1)
        set(value) = preferences.edit { putLong(INPUT_METHOD_LAST_GAME_ID, value) }
    var keyboardSelectedNumber
        get() = preferences.getInt(INPUT_METHOD_SELECTED_NUMBER, 0)
        set(value) = preferences.edit { putInt(INPUT_METHOD_SELECTED_NUMBER, value) }
    var keyboardEditMode
        get() = preferences.getInt(INPUT_METHOD_EDIT_MODE, InputMethod.MODE_EDIT_VALUE)
        set(value) = preferences.edit { putInt(INPUT_METHOD_EDIT_MODE, value) }

    // Long
    var recentlyPlayedPuzzleId
        get() = preferences.getLong(MOST_RECENTLY_PLAYED_PUZZLE_ID, 0)
        set(value) = preferences.edit { putLong(MOST_RECENTLY_PLAYED_PUZZLE_ID, value) }

    // Boolean
    val fillInMarksEnabled
        get() = preferences.getBoolean(FILL_IN_MARKS_ENABLED, false)
    var bellEnabled
        get() = preferences.getBoolean(BELL_ENABLED, false)
        set(value) = preferences.edit { putBoolean(BELL_ENABLED, value) }
    val highlightWrongValues
        get() = WrongValueHighlightMode.fromString(preferences.getString(HIGHLIGHT_WRONG_VALUES, ""))
    val isDoubleMarksEnabled
        get() = preferences.getBoolean(DOUBLE_MARKS_ENABLED, true)
    val highlightTouchedCell
        get() = preferences.getBoolean(HIGHLIGHT_TOUCHED_CELL, true)
    val isRemoveMarksOnInput
        get() = preferences.getBoolean(REMOVE_MARKS_ON_INPUT, true)
    val isShowTime
        get() = preferences.getBoolean(SHOW_TIME, true)
    val isShowMistakeCounter
        get() = preferences.getBoolean(SHOW_MISTAKE_COUNTER, true)
    val isHighlightCompletedValues
        get() = preferences.getBoolean(HIGHLIGHT_COMPLETED_VALUES, true)
    val isMoveCellSelectionOnPress
        get() = preferences.getBoolean(IM_MOVE_RIGHT_ON_INSERT, false)
    val selectedInputModes
        get() = preferences.getStringSet(SELECTED_INPUT_MODES, setOf(IM_POPUP, INSERT_ON_TAP, SELECT_ON_TAP))
    val isPopupEnabled
        get() = selectedInputModes?.contains(IM_POPUP) != false
    val isInsertOnTapEnabled
        get() = selectedInputModes?.contains(INSERT_ON_TAP) != false
    val isSelectOnTapEnabled
        get() = selectedInputModes?.contains(SELECT_ON_TAP) != false
    val isBidirectionalSelection
        get() = preferences.getBoolean(BIDIRECTIONAL_SELECTION, true)
    val isHighlightSimilar
        get() = preferences.getBoolean(HIGHLIGHT_SIMILAR, true)
    val isShowNumberTotals
        get() = preferences.getBoolean(SHOW_NUMBER_TOTALS, false)
    val doubleMarksEnabled
        get() = preferences.getBoolean(DOUBLE_MARKS_ENABLED, true)
    val isShowHints
        get() = preferences.getBoolean(SHOW_HINTS, true)
    var isShowNotStarted
        get() = preferences.getBoolean(FILTER_STATE_NOT_STARTED, true)
        set(value) = preferences.edit { putBoolean(FILTER_STATE_NOT_STARTED, value) }
    var isShowPlaying
        get() = preferences.getBoolean(FILTER_STATE_PLAYING, true)
        set(value) = preferences.edit { putBoolean(FILTER_STATE_PLAYING, value) }
    var isShowCompleted
        get() = preferences.getBoolean(FILTER_STATE_SOLVED, true)
        set(value) = preferences.edit { putBoolean(FILTER_STATE_SOLVED, value) }
    var isSortAscending
        get() = preferences.getBoolean(SORT_ORDER, false)
        set(value) = preferences.edit { putBoolean(SORT_ORDER, value) }
    val isShowPuzzleListOnStartup
        get() = preferences.getBoolean(SHOW_PUZZLE_LISTS_ON_STARTUP, false)

    // String
    val theme
        get() = preferences.getString(THEME, OPENSUDOKU) ?: OPENSUDOKU
    var uiMode
        get() = preferences.getString(CUSTOM_THEME_UI_MODE, SYSTEM) ?: SYSTEM
        set(value) = preferences.edit { putString(CUSTOM_THEME_UI_MODE, value) }

    // Custom types
    val importStrategy
        get() = ImportStrategyTypes.convertKey2Type(preferences.getString(Tag.IMPORT_STRATEGY, ""))

    val sameValueHighlightMode: SameValueHighlightMode
        get() {
            return if (preferences.getBoolean(HIGHLIGHT_SIMILAR_CELLS, true)) {
                SameValueHighlightMode.fromString(preferences.getString(HIGHLIGHT_SIMILAR_MARKS_MODE, VALUES))
            } else {
                SameValueHighlightMode.NONE
            }
        }

    fun registerOnSharedPreferenceChangeListener(function: SharedPreferences. OnSharedPreferenceChangeListener) {
        preferences.registerOnSharedPreferenceChangeListener(function)
    }

    fun unregisterOnSharedPreferenceChangeListener(function: SharedPreferences. OnSharedPreferenceChangeListener) {
        preferences.unregisterOnSharedPreferenceChangeListener(function)
    }
}

enum class WrongValueHighlightMode {
    OFF,
    DIRECT,
    INDIRECT;

    companion object {
        fun fromString(string: String?): WrongValueHighlightMode {
            return when (string) {
                "off" -> OFF
                "direct" -> DIRECT
                else -> INDIRECT
            }
        }
    }
}

enum class SameValueHighlightMode {
    NONE,
    NUMBERS,
    NUMBERS_AND_MARK_VALUES,
    NUMBERS_AND_MARK_CELLS,
    HINTS_ONLY;

    companion object {
        fun fromString(string: String?): SameValueHighlightMode {
            return when (string) {
                "values" -> NUMBERS_AND_MARK_VALUES
                "cells" -> NUMBERS_AND_MARK_CELLS
                else -> NUMBERS
            }
        }
    }
}
