/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

/** Strategy: Naked Single
 */
class NextStepNakedSingle(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		return checkForNakedSingle()
	}

	/** Strategy: Naked Single
	 *
	 * Find a cell with only one candidate
	 *
	 * Message:
	 * 			"Naked Single: {5} -> [ r5c2 ]"
	 * 			"-> enter value {5} into [ r5c2 ]"
	 */
	private fun checkForNakedSingle(): Boolean {

		forEachHouse@ board.cells.forEach {
			it.forEach forEachCell@{ cell ->
				if (cell.value > 0) return@forEachCell // cell has already a value
				if (cell.centralMarks.marksValues.size != 1) return@forEachCell // not only one candidate
				val candidates = cell.centralMarks.marksValues
				val strategyName = context.getString(R.string.hint_strategy_naked_single)
				when(hintLevel) {
					HintLevels.LEVEL1 -> { nextStepText = strategyName }
					HintLevels.LEVEL2 -> { nextStepText = "$strategyName: {${candidates[0]}}" }
					HintLevels.LEVEL3 -> {
						nextStepText =  "$strategyName: {${candidates[0]}}" +
							" \u27A0 [${cell.gridAddress}]"
						cellsToHighlight[HintHighlight.REGION] = listOf(cell).map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = listOf(cell).map { it.rowIndex to it.columnIndex }
					}
					HintLevels.LEVEL4 -> {
						nextStepText =  "$strategyName: {${candidates[0]}}" +
							" \u27A0 [${cell.gridAddress}]" + "\n" +
							context.getString(R.string.hint_enter_value_into,
								"{${candidates[0]}}", "[${cell.gridAddress}]")
						cellsToHighlight[HintHighlight.REGION] = listOf(cell).map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = listOf(cell).map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.TARGET] = listOf(cell).map { it.rowIndex to it.columnIndex }
					}
				}
				return@checkForNakedSingle true
			} // forEachCell@
		} // forEachHouse@
		return false
	}

}

