/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

/** Strategy: Full House / Last Digit
 */
class NextStepFullHouse(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		return checkForFullHouse()
	}

	/** Strategy: Full House / Last Digit
	 *
	 * Find last cell in a house (row, column or box) without a value
	 *
	 * Message:
	 * 			"Full House: {4} -> (b5)"
	 * 		    "-> enter value {4} into [ r5c5 ]"
	 */
	private fun checkForFullHouse(): Boolean {

		// loop over all house types
		forHouseType@ for (houseType in HouseTypes.entries) {

			// get all houses for the house type
			val houseCellsArray = when (houseType) {
				HouseTypes.ROW -> board.getHousesRows()
				HouseTypes.COL -> board.getHousesColumns()
				HouseTypes.BOX -> board.getHousesSectors()
			}

			// loop over all houses
			forHouse@ for (houseCells in houseCellsArray) {
				val cellsToCheck = houseCells.cells.filter { it.value == 0 }
				if (cellsToCheck.size != 1) continue@forHouse
				val emptyCell = cellsToCheck.elementAt(0)
				val strategyName = context.getString(R.string.hint_strategy_full_house)
				when(hintLevel) {
					HintLevels.LEVEL1 -> { nextStepText = strategyName }
					HintLevels.LEVEL2 -> { nextStepText = "$strategyName: {${emptyCell.solution}}" }
					HintLevels.LEVEL3 -> {
						nextStepText = "$strategyName: {${emptyCell.solution}}" +
							" \u27A0 (${houseType.houseAdr(emptyCell)})"
						cellsToHighlight[HintHighlight.REGION] = houseCells.cells.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = listOf(emptyCell).map { it.rowIndex to it.columnIndex }
					}
					HintLevels.LEVEL4 -> {
						nextStepText = "$strategyName: {${emptyCell.solution}}" +
							" \u27A0 (${houseType.houseAdr(emptyCell)})" + "\n" +
							context.getString(R.string.hint_enter_value_into,
								"{${emptyCell.solution}}", "[${emptyCell.gridAddress}]")
						cellsToHighlight[HintHighlight.REGION] = houseCells.cells.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = listOf(emptyCell).map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.TARGET] = listOf(emptyCell).map { it.rowIndex to it.columnIndex }
					}
				}
				return true
			} // forHouse@
		} // forHouseType@
		return false
	}

}

