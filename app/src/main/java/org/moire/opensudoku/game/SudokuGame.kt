/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.game

import android.content.ContentValues
import android.os.SystemClock
import org.moire.opensudoku.db.Column
import org.moire.opensudoku.game.command.AbstractCommand
import org.moire.opensudoku.game.command.ClearAllMarksCommand
import org.moire.opensudoku.game.command.CommandStack
import org.moire.opensudoku.game.command.EditCellCentralMarksCommand
import org.moire.opensudoku.game.command.EditCellCornerMarksCommand
import org.moire.opensudoku.game.command.FillInMarksCommand
import org.moire.opensudoku.game.command.FillInMarksWithAllValuesCommand
import org.moire.opensudoku.game.command.SetCellValueAndRemoveMarksCommand
import org.moire.opensudoku.game.command.SetCellValueCommand
import java.time.Instant

class SudokuGame(board: SudokuBoard) {
	var id: Long = -1
	var folderId: Long = -1
	var created: Long = 0
	var state: Int
	var mistakeCounter: Int = 0
	var userNote: String = ""
	private var isSolverUsed = false
	internal var isRemoveMarksOnEntry = true
	internal var isCheckIndirectErrors: Boolean = true
	internal var onPuzzleSolvedListener: (() -> Unit)? = null
	internal var onDigitFinishedManuallyListener: ((Int) -> Unit)? = null
	internal var onInvalidDigitEnteredListener: (() -> Unit)? = null
	private var activeFromTime: Long = -1 // time when current activity has become active.

	var board: SudokuBoard = board
		internal set(newCells) {
			field = newCells
			newCells.validate()
			commandStack = CommandStack(newCells)
		}

	var onHasUndoChangedListener: (() -> Unit)? = null
		set(newValue) {
			field = newValue
			commandStack.onEmptyChangeListener = newValue
		}

	var commandStack: CommandStack = CommandStack(this.board)
		set(value) {
			field = value
			field.onEmptyChangeListener = onHasUndoChangedListener
		}

	private val isPaused: Boolean
		get() = activeFromTime == -1L

	var lastPlayed: Long = 0
		get() = (if (isPaused) field else Instant.now().toEpochMilli())

	/**
	 * Time of game-play in milliseconds.
	 */
	var playingDuration: Long = 0
		get() = (if (isPaused) field else field + SystemClock.uptimeMillis() - activeFromTime)
		set(value) {
			field = value
			if (!isPaused) {
				activeFromTime = SystemClock.uptimeMillis()
			}
		}

	val contentValues: ContentValues
		get() {
			return ContentValues().apply {
				put(Column.ORIGINAL_VALUES, board.originalValues)
				put(Column.CELLS_DATA, board.serialize())
				put(Column.CREATED, created)
				put(Column.LAST_PLAYED, lastPlayed)
				put(Column.STATE, state)
				put(Column.MISTAKE_COUNTER, mistakeCounter)
				put(Column.TIME, playingDuration)
				put(Column.USER_NOTE, userNote)
				put(Column.COMMAND_STACK, commandStack.serialize())
				put(Column.FOLDER_ID, folderId)
			}
		}

	init {
		state = GAME_STATE_NOT_STARTED
	}

	/**
	 * Sets value for the given cell. 0 means empty cell.
	 */
	fun setCellValue(cell: Cell, value: Int, isManual: Boolean) {
		if (cell.value == value || (!cell.isEditable && !board.isEditMode)) return
		require(!(value < 0 || value > 9)) { @Suppress("HardCodedStringLiteral") "Value must be between 0-9." }

		if (isRemoveMarksOnEntry) {
			executeCommand(SetCellValueAndRemoveMarksCommand(cell, value), isManual)
		} else {
			executeCommand(SetCellValueCommand(cell, value), isManual)
		}

		if (board.isEditMode) {
			cell.isEditable = false
		}

		board.validate()

		if (isManual && value > 0) {
			val isCorrect = if (isCheckIndirectErrors) cell.isCorrect else cell.isValid
			if (!isCorrect) {
				onInvalidDigitEnteredListener?.invoke()
				mistakeCounter += 1
			} else if (board.valuesUseCount[value] == 9 && !board.hasMistakes) {
				onDigitFinishedManuallyListener?.invoke(value)
			}
		}

		if (isSolved) {
			markGameAsCompletedAndPause()
			onPuzzleSolvedListener?.invoke()
		}
	}

	/**
	 * Sets central marks attached to the given cell.
	 */
	fun setCellCentralMarks(cell: Cell, marks: CellMarks, isManual: Boolean): Boolean {
		if (cell.isEditable && cell.centralMarks != marks) {
			executeCommand(EditCellCentralMarksCommand(cell, marks), isManual)
			return true
		}
		return false
	}

	/**
	 * Sets corner marks attached to the given cell.
	 */
	fun setCellCornerMarks(cell: Cell, marks: CellMarks, isManual: Boolean): Boolean {
		if (cell.isEditable && cell.cornerMarks != marks) {
			executeCommand(EditCellCornerMarksCommand(cell, marks), isManual)
			return true
		}
		return false
	}

	private fun executeCommand(c: AbstractCommand, isManual: Boolean) {
		commandStack.execute(c, isManual)
	}

	/**
	 * Undo last command.
	 */
	fun undo(): Cell? = commandStack.undo()

	fun hasSomethingToUndo(): Boolean = !commandStack.isEmpty

	fun setUndoCheckpoint() {
		commandStack.setCheckpoint()
	}

	fun undoToCheckpoint() {
		commandStack.undoToCheckpoint()
	}

	fun hasUndoCheckpoint(): Boolean = commandStack.hasCheckpoint()

	fun undoToBeforeMistake() {
		commandStack.undoToSolvableState()
	}

	val lastCommandCell: Cell?
		get() = commandStack.lastCommandCell

	/**
	 * Start game-play.
	 */
	fun start() {
		state = GAME_STATE_PLAYING
		resume()
	}

	fun resume() {
		// reset time we have spent playing so far, so time when activity was not active
		// will not be part of the game play time
		activeFromTime = SystemClock.uptimeMillis()
	}

	/**
	 * Pauses game-play (for example if activity pauses).
	 */
	fun pause() {
		// save time we have spent playing so far - it will be reset after resuming
		val lastPlayingDuration = playingDuration // getter calculated value
		activeFromTime = -1L
		playingDuration = lastPlayingDuration
		lastPlayed = Instant.now().toEpochMilli()
	}

	val solutionCount: Int
		get() = board.solutionCount

	/**
	 * Solves puzzle from original state
	 */
	fun solve(): Int {
		isSolverUsed = true
		if (board.solutionCount != 1) {
			return board.solutionCount
		}
		board.cells.forEach { row ->
			row.forEach { cell ->
				setCellValue(cell, cell.solution, false)
			}
		}
		commandStack.clean()
		return 1
	}

	fun usedSolver(): Boolean = isSolverUsed

	/**
	 * Solves puzzle and fills in correct value for selected cell
	 */
	fun solveCell(cell: Cell) {
		require(board.solutionCount == 1) {
			@Suppress("HardCodedStringLiteral")
			"This puzzle has " + board.solutionCount + " solutions"
		}
		setCellValue(cell, cell.solution, true)
	}

	/**
	 * Pauses game-play and sets state to completed. Called when puzzle is solved.
	 */
	private fun markGameAsCompletedAndPause() {
		pause()
		state = GAME_STATE_COMPLETED
	}

	/**
	 * Resets game.
	 */
	fun reset() {
		board.cells.forEach { row ->
			row.forEach { cell ->
				if (cell.isEditable) {
					cell.value = 0
				}
				cell.centralMarks = CellMarks()
				cell.cornerMarks = CellMarks()
			}
		}
		commandStack = CommandStack(board)
		board.validate()
		mistakeCounter = 0
		playingDuration = 0
		lastPlayed = 0
		state = GAME_STATE_NOT_STARTED
		isSolverUsed = false
	}

	/**
	 * Returns true, if puzzle is solved. In order to know the current state, you have to call validate first.
	 */
	private val isSolved: Boolean
		get() = board.isSolved

	fun clearAllMarksManual() = executeCommand(ClearAllMarksCommand(), true)

	/**
	 * Fills in possible values which can be entered in each cell.
	 */
	fun fillInMarksManual() = executeCommand(FillInMarksCommand(), true)

	/**
	 * Fills in all values which can be entered in each cell.
	 */
	fun fillInMarksWithAllValuesManual() = executeCommand(FillInMarksWithAllValuesCommand(), true)

	companion object {
		const val GAME_STATE_PLAYING = 0
		const val GAME_STATE_NOT_STARTED = 1
		const val GAME_STATE_COMPLETED = 2
		fun createEmptyGame(isEditMode: Boolean): SudokuGame {
			val game = SudokuGame(SudokuBoard.createEmpty(isEditMode))
			game.created = Instant.now().toEpochMilli() // set creation time
			return game
		}
	}
}
