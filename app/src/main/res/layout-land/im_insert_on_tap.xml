<?xml version="1.0" encoding="utf-8"?>

<!--
  ~ This file is part of Open Sudoku - an open-source Sudoku game.
  ~ Copyright (C) 2009-2024 by original authors.
  ~
  ~ This program is free software: you can redistribute it and/or modify
  ~ it under the terms of the GNU General Public License as published by
  ~ the Free Software Foundation, either version 3 of the License, or
  ~ (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful,
  ~ but WITHOUT ANY WARRANTY; without even the implied warranty of
  ~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  ~ GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License
  ~ along with this program.  If not, see <https://www.gnu.org/licenses/>.
  -->

<androidx.constraintlayout.widget.ConstraintLayout xmlns:android="http://schemas.android.com/apk/res/android"
	xmlns:app="http://schemas.android.com/apk/res-auto"
	xmlns:tools="http://schemas.android.com/tools"
	android:layout_width="match_parent"
	android:layout_height="match_parent"
	>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_1"
		style="@style/keypadButton"
		android:text="1"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintHorizontal_chainStyle="packed"
		app:layout_constraintVertical_chainStyle="packed"
		app:layout_constraintVertical_bias="1.0"
		app:layout_constraintHorizontal_bias="1.0"
		app:layout_constraintStart_toStartOf="parent"
		app:layout_constraintEnd_toStartOf="@id/button_2"
		app:layout_constraintTop_toTopOf="parent"
		app:layout_constraintBottom_toTopOf="@id/button_4"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_2"
		style="@style/keypadButton"
		android:text="2"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toEndOf="@id/button_1"
		app:layout_constraintEnd_toStartOf="@id/button_3"
		app:layout_constraintTop_toTopOf="parent"
		app:layout_constraintBottom_toBottomOf="@id/button_1"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_3"
		style="@style/keypadButton"
		android:text="3"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toEndOf="@id/button_2"
		app:layout_constraintEnd_toStartOf="@id/button_clear"
		app:layout_constraintTop_toTopOf="parent"
		app:layout_constraintBottom_toBottomOf="@id/button_2"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_4"
		style="@style/keypadButton"
		android:text="4"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_1"
		app:layout_constraintEnd_toEndOf="@id/button_1"
		app:layout_constraintTop_toBottomOf="@id/button_1"
		app:layout_constraintBottom_toTopOf="@id/button_7"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_5"
		style="@style/keypadButton"
		android:text="5"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_2"
		app:layout_constraintEnd_toEndOf="@id/button_2"
		app:layout_constraintTop_toBottomOf="@id/button_2"
		app:layout_constraintBottom_toTopOf="@id/button_8"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_6"
		style="@style/keypadButton"
		android:text="6"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_3"
		app:layout_constraintEnd_toEndOf="@id/button_3"
		app:layout_constraintTop_toBottomOf="@id/button_1"
		app:layout_constraintBottom_toTopOf="@id/button_9"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_7"
		style="@style/keypadButton"
		android:text="7"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_4"
		app:layout_constraintEnd_toEndOf="@id/button_4"
		app:layout_constraintTop_toBottomOf="@id/button_4"
		app:layout_constraintBottom_toTopOf="@id/enter_number"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_8"
		style="@style/keypadButton"
		android:text="8"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_5"
		app:layout_constraintEnd_toEndOf="@id/button_5"
		app:layout_constraintTop_toBottomOf="@id/button_5"
		app:layout_constraintBottom_toTopOf="@id/central_mark"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.NumberButton
		android:id="@+id/button_9"
		style="@style/keypadButton"
		android:text="9"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_6"
		app:layout_constraintEnd_toEndOf="@id/button_6"
		app:layout_constraintTop_toBottomOf="@id/button_4"
		app:layout_constraintBottom_toTopOf="@id/corner_mark"
		tools:ignore="HardcodedText"
		/>

	<org.moire.opensudoku.gui.IconButton
		android:id="@+id/enter_number"
		style="@style/keypadButton"
		android:contentDescription="@string/cell_value"
		app:icon="@drawable/ic_enter_number"
		app:iconGravity="textStart"
		app:iconPadding="0dp"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_7"
		app:layout_constraintEnd_toEndOf="@id/button_7"
		app:layout_constraintTop_toBottomOf="@id/button_7"
		app:layout_constraintBottom_toBottomOf="parent"
		/>

	<org.moire.opensudoku.gui.IconButton
		android:id="@+id/central_mark"
		style="@style/keypadButton"
		android:contentDescription="@string/central_marks"
		app:icon="@drawable/ic_central_mark"
		app:iconGravity="textStart"
		app:iconPadding="0dp"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintEnd_toEndOf="@id/button_8"
		app:layout_constraintStart_toStartOf="@id/button_8"
		app:layout_constraintTop_toBottomOf="@id/button_8"
		app:layout_constraintBottom_toBottomOf="parent"
		/>

	<org.moire.opensudoku.gui.IconButton
		android:id="@+id/corner_mark"
		style="@style/keypadButton"
		android:contentDescription="@string/corner_mark"
		app:icon="@drawable/ic_corner_mark"
		app:iconGravity="textStart"
		app:iconPadding="0dp"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toStartOf="@id/button_9"
		app:layout_constraintEnd_toEndOf="@id/button_9"
		app:layout_constraintTop_toBottomOf="@id/button_7"
		app:layout_constraintBottom_toBottomOf="parent"
		/>

	<org.moire.opensudoku.gui.IconButton
		android:id="@+id/button_clear"
		style="@style/keypadButton"
		app:icon="@drawable/ic_eraser"
		app:iconGravity="textStart"
		app:iconPadding="0dp"
		app:layout_constraintDimensionRatio="1:1"
		app:layout_constraintStart_toEndOf="@id/button_3"
		app:layout_constraintEnd_toEndOf="parent"
		app:layout_constraintTop_toTopOf="parent"
		app:layout_constraintBottom_toBottomOf="@id/button_3"
		/>

	<Button
		android:id="@+id/single_number_switch_input_mode"
		style="@style/Widget.Material3.Button.TonalButton"
		android:layout_width="0dp"
		android:layout_height="0dp"
		app:cornerRadius="4dp"
		android:textSize="16sp"
		android:padding="0dp"
		android:insetLeft="4dp"
		android:insetRight="4dp"
		app:layout_constraintDimensionRatio="1:3"
		app:layout_constraintStart_toStartOf="@id/button_clear"
		app:layout_constraintEnd_toEndOf="@id/button_clear"
		app:layout_constraintTop_toBottomOf="@id/button_clear"
		app:layout_constraintBottom_toBottomOf="parent"
		/>
</androidx.constraintlayout.widget.ConstraintLayout>
